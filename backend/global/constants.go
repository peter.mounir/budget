package global

const (
	ACTION_CREATE 		int = 0
	ACTION_UPDATE 		int = 1
	ACTION_DELETE 		int = 2
)

const (
	DATA_RECEIPT  		int = 1
	DATA_SEARCH_PRODUCT int = 2
	DATA_SEARCH_STORE 	int = 3
	DATA_ACTIVITY 		int = 4
	DATA_QUESTIONNAIRE 	int = 5
	DATA_EXPERIENCE 	int = 6
	DATA_PARADATA 		int = 7
)

const (
	GROUP_SUPERUSER 	string = "superuser"
	GROUP_MONITORING	string = "monitoring"
	GROUP_CMM			string = "casemanagement" // cmm stands for casemanagement
	GROUP_CBS_PROCESS 	string = "CBS Process"
)

const (
	STATUS_ENABLED 		string = "enabled"
	STATUS_DISABLED 	string = "disabled"
)

