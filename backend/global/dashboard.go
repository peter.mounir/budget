package global

type Parameter struct {
	Name 		string  `json:"name"`
	Value	 	string  `json:"value"`
}

type DashboardBody struct {
	Superuser	*User		`json:"superuser"`
	Group		*Group		`json:"group"`
	User		*User		`json:"user"`
	Parameters	[]Parameter	`json:"parameters"`
}

type ReceiptsPerDay struct {
	GroupName	string		`json:"groupName"`
	UserName	string 		`json:"userName"`
	Date		int			`json:"date"`
	Receipts	int64		`json:"receipts"`
}

type ReceiptsPerDayData struct {
	ReceiptsPerDays	[]ReceiptsPerDay	`json:"receiptsPerDays"`
}

type ReceiptsPerPhone struct {
	GroupName	string		`json:"groupName"`
	UserName	string 		`json:"userName"`
	PhoneName	string 		`json:"phoneName"`
	PhoneType	string 		`json:"phoneType"`
	PhoneManufacturer	string 		`json:"phoneManufacturer"`
	PhoneModel	string 		`json:"phoneModel"`
	SyncOrder	int64		`json:"syncOrder"`
	Receipts	int64		`json:"receipts"`
	Products	int64		`json:"products"`
	Photos		int64		`json:"photos"`
}

type ReceiptsPerPhoneData struct {
	ReceiptsPerPhones	[]ReceiptsPerPhone	`json:"receiptsPerPhones"`
}

type ReceiptsPerUser struct {
	GroupName	string		`json:"groupName"`
	UserName	string 		`json:"userName"`
	FirstDay	int 		`json:"firstDay"`
	LastDay		int 		`json:"lastDay"`
	Days		int64 		`json:"days"`
	Receipts	int64 		`json:"receipts"`
	Photos		int64		`json:"photos"`
	TotalPrice	float32		`json:"totalPrice"`
}

type ReceiptsPerUserData struct {
	ReceiptsPerUsers	[]ReceiptsPerUser	`json:"receiptsPerUsers"`
}

type UserReceiptsData struct {
	ReceiptsData	[]ReceiptData	`json:"receiptsData"`
}
