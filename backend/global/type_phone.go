package global

type Phone struct {
	Id          int64     `json:"id"`
	User_Id  	int64     `json:"userId"`
	Name        string    `json:"name"`
	Sync_Order 	int64     `json:"syncOrder"`
}

type Phone_Info struct {
	Id          int64     `json:"id"`
	Phone_Id    int64	  `json:"phoneId"`
	Key        	string    `json:"key"`
	Value    	string    `json:"value"`
}

type PhoneBody struct {
	User		*User			`json:"user"`
	Phone		*Phone			`json:"phone"`
	Phone_Infos	[]Phone_Info	`json:"phoneInfos"`
}

type PhoneData struct {
	User		*User			`json:"user"`
	Phone		*Phone			`json:"phone"`
	Group_Infos	[]Group_Info	`json:"groupInfos"`
}
