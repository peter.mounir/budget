package main

import (
	"backend/global"
	"backend/database"
	"backend/restapi"
	"log"
	"net/http"
	"strconv"
	"github.com/gorilla/mux"
)

const STOPONMISSINGENVIRONMENT bool = false

func main() {

	var port 		string = global.GetEnvironment("PORT"		, STOPONMISSINGENVIRONMENT)
	var host 		string = global.GetEnvironment("DB_HOST"	, STOPONMISSINGENVIRONMENT)
	var dbname 		string = global.GetEnvironment("DB_NAME"	, STOPONMISSINGENVIRONMENT)
	var dbport 		string = global.GetEnvironment("DB_PORT"	, STOPONMISSINGENVIRONMENT)
	var user 		string = global.GetEnvironment("DB_USER"	, STOPONMISSINGENVIRONMENT)
	var password 	string = global.GetEnvironment("DB_PASSWORD", STOPONMISSINGENVIRONMENT)

	var dbportInt int
	dbportInt, _ = strconv.Atoi(dbport)

	database.Init(host, dbportInt, user, password, dbname)

	router := mux.NewRouter()
	
	router.HandleFunc("/budget/test"    					, restapi.Test_WebServiceA).Methods("GET")
	router.HandleFunc("/budget/testb"    					, restapi.Test_WebServiceB).Methods("GET")
	router.HandleFunc("/budget/testc"    					, restapi.Test_WebServiceC).Methods("GET")
	router.HandleFunc("/loaderio-d88cc05dd46717f147e8dddd74a2be24.txt"	, restapi.Test_Loaderio).Methods("GET")   

	router.HandleFunc("/budget/group/list"  				, restapi.Group_List).Methods("POST")
	router.HandleFunc("/budget/group/create"  				, restapi.Group_Create).Methods("POST")
	router.HandleFunc("/budget/group/enable"  				, restapi.Group_Enable).Methods("POST")
	router.HandleFunc("/budget/group/disable"  				, restapi.Group_Disable).Methods("POST")
	router.HandleFunc("/budget/group/delete"  				, restapi.Group_Delete).Methods("POST")
	router.HandleFunc("/budget/group/rename"  				, restapi.Group_Rename).Methods("POST")
	router.HandleFunc("/budget/group/infos"		  			, restapi.Group_Infos).Methods("POST")

	router.HandleFunc("/budget/users/list"					, restapi.Users_List).Methods("POST")
	router.HandleFunc("/budget/users/listall"				, restapi.Users_List_All).Methods("POST")
	router.HandleFunc("/budget/users/create"				, restapi.Users_Create).Methods("POST")
	router.HandleFunc("/budget/users/delete"  				, restapi.Users_Delete).Methods("POST")
	router.HandleFunc("/budget/users/regroupgroup"			, restapi.Users_Regroup_Group).Methods("POST")
	router.HandleFunc("/budget/users/regroupusers"			, restapi.Users_Regroup_Users).Methods("POST")
	router.HandleFunc("/budget/users/passwords"				, restapi.Users_Passwords).Methods("POST")
	router.HandleFunc("/budget/users/generate"				, restapi.Users_Generate).Methods("POST")

	router.HandleFunc("/budget/phone/register"				, restapi.Phone_Register).Methods("POST")

	router.HandleFunc("/budget/data/pushreceipt"  			, restapi.Data_Push_Receipt).Methods("POST")
	router.HandleFunc("/budget/data/pullreceipt"  			, restapi.Data_Pull_Receipt).Methods("POST")
	router.HandleFunc("/budget/data/pushproduct"  			, restapi.Data_Push_SearchProduct).Methods("POST")
	router.HandleFunc("/budget/data/pullproduct"  			, restapi.Data_Pull_SearchProduct).Methods("POST")
	router.HandleFunc("/budget/data/pushstore"  			, restapi.Data_Push_SearchStore).Methods("POST")
	router.HandleFunc("/budget/data/pullstore"	  			, restapi.Data_Pull_SearchStore).Methods("POST")
	router.HandleFunc("/budget/data/pushparadata"  			, restapi.Data_Push_Paradata).Methods("POST")
	router.HandleFunc("/budget/data/pushactivity"  			, restapi.Data_Push_Activity).Methods("POST")
	router.HandleFunc("/budget/data/pullactivity"			, restapi.Data_Pull_Activity).Methods("POST")
	router.HandleFunc("/budget/data/pushquestionnaire"  	, restapi.Data_Push_Questionnaire).Methods("POST")
	router.HandleFunc("/budget/data/pullquestionnaire"		, restapi.Data_Pull_Questionnaire).Methods("POST")
	router.HandleFunc("/budget/data/pushexperience"  		, restapi.Data_Push_Experience).Methods("POST")
	router.HandleFunc("/budget/data/pullexperience"			, restapi.Data_Pull_Experience).Methods("POST")
	
	router.HandleFunc("/budget/process/pullreceipt"  		, restapi.Process_Pull_ReceiptForProcess).Methods("POST")
	router.HandleFunc("/budget/process/pushreceipt"  		, restapi.Process_Push_ReceiptForProcess).Methods("POST")
	

	router.HandleFunc("/budget/dashboard/receiptsperday"  	, restapi.Dashboard_ReceiptsPerDay).Methods("POST")
	router.HandleFunc("/budget/dashboard/receiptsperphone" 	, restapi.Dashboard_ReceiptsPerPhone).Methods("POST")
	router.HandleFunc("/budget/dashboard/receiptsperuser" 	, restapi.Dashboard_ReceiptsPerUser).Methods("POST")
	router.HandleFunc("/budget/dashboard/userreceipts" 		, restapi.Dashboard_UserReceipts).Methods("POST")
	router.HandleFunc("/budget/dashboard/paradatadatetime" 	, restapi.Dashboard_ParadataDateTime).Methods("POST")
	router.HandleFunc("/budget/dashboard/paradatascreentime", restapi.Dashboard_ParadataScreenTime).Methods("POST")
	router.HandleFunc("/budget/dashboard/paradataclick"		, restapi.Dashboard_ParadataClick).Methods("POST")

	router.HandleFunc("/budget/debug/receiptatindex"		, restapi.Debug_ReceiptAtIndex).Methods("POST")
	
	log.Fatal(http.ListenAndServe(":"+port, router))
}

