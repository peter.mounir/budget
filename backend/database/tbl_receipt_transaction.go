package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"backend/global"
)

func Create_Transaction(tx *sql.Tx, sync_id int64, t *global.Transaction) error {
	sqlStatement := "" +
		"INSERT INTO tbl_receipt_transaction (sync_id, transaction_id, store, store_type, date, " +
		"	discount_amount, discount_percentage, discount_text, expense_abroad, expense_online, " +
		"	total_price, receipt_location, receipt_producttype) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) "

	_, err = tx.Exec(sqlStatement, sync_id,
		t.TransactionID, t.Store, t.StoreType, t.Date,
		t.DiscountAmount, t.DiscountPercentage, t.DiscountText, t.ExpenseAbroad, t.ExpenseOnline,
		t.TotalPrice, t.ReceiptLocation, t.ReceiptProductType)
	if err != nil {
		return err
	}

	global.MyPrint("& Inserted Transaction")
	return nil
}

func Delete_Transaction(tx *sql.Tx, sync_id int64) error {
	sqlStatement := "" +
		"DELETE FROM tbl_receipt_transaction " +
		"WHERE sync_id=$1 "

	_, err = tx.Exec(sqlStatement, sync_id)
	if err != nil {
		return err
	}
	
	global.MyPrint("& Deleted Transaction")
	return nil
}

func Exists_Transaction(sync_id int64) (bool, error) {
	var exists bool

	sqlStatement := "" +
		"SELECT EXISTS " +
		"(SELECT sync_id FROM tbl_receipt_transaction WHERE sync_id=$1) "

	err := con.QueryRow(sqlStatement, sync_id).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}
	return exists, nil
}

func scan_Transaction_Row(rows *sql.Rows) global.Transaction {
	var sync_id int64
	var transactionID string
	var store string
	var storeType string
	var date int
	var discountAmount string
	var discountPercentage string
	var discountText string
	var expenseAbroad string
	var expenseOnline string
	var totalPrice float32
	var receiptLocation string
	var receiptProductType string
	err = rows.Scan(&sync_id, &transactionID, &store, &storeType, &date,
		&discountAmount, &discountPercentage, &discountText, &expenseAbroad,
		&expenseOnline, &totalPrice, &receiptLocation, &receiptProductType)
	if err != nil {
		panic(err)
	}
	return global.Transaction{
		TransactionID:      transactionID,
		Store:              store,
		StoreType:          storeType,
		Date:               date,
		DiscountAmount:     discountAmount,
		DiscountPercentage: discountPercentage,
		DiscountText:       discountText,
		ExpenseAbroad:      expenseAbroad,
		ExpenseOnline:      expenseOnline,
		TotalPrice:         totalPrice,
		ReceiptLocation:    receiptLocation,
		ReceiptProductType: receiptProductType}
}

func Select_Transaction(sync_id int64) (global.Transaction, error) {
	var transaction global.Transaction

	sqlStatement := "" +
		"SELECT sync_id, transaction_id, store, store_type, date, " +
		"	discount_amount, discount_percentage, discount_text, expense_abroad, expense_online, " +
		"	total_price, receipt_location, receipt_producttype " +
		"FROM tbl_receipt_transaction " +
		"WHERE sync_id=$1 "

	rows, err := con.Query(sqlStatement, sync_id)
	if err != nil {
		return transaction, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		transaction = scan_Transaction_Row(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return transaction, err
	}

	if found {
		return transaction, nil
	} else {
		return transaction, errors.New("Error: Transaction not found!")
	}
}
