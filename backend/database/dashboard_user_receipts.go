package database

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"backend/global"
)

func scan_SyncID_Row(rows *sql.Rows) int64 {
	var id int64

	err = rows.Scan(&id)
	if err != nil {
		panic(err)
	}

	return id
}

func select_UserReceipts(user_id int64) ([]global.ReceiptData, error) {
	var receiptsData []global.ReceiptData
	
	sqlStatement := "" +	
	"SELECT id " +
	"FROM public.tbl_sync " +
	"where user_id = $1 and data_type = 1 and action <> 2 "

	rows, err := con.Query(sqlStatement, user_id)
	if err != nil {
		return receiptsData, err
	}
	defer rows.Close()
	
	var found bool = false
	for rows.Next() {
		receiptsData = append(receiptsData, GetReceiptData(scan_SyncID_Row(rows)))
		found = true
	}

	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return receiptsData, err
	}

	if found {
		return receiptsData, nil
	} else {
		return receiptsData, errors.New("Error: No ReceiptsData foud")
	}
}

func Get_UserReceipts(user_id int64) (global.UserReceiptsData, error) {
	var receiptsData []global.ReceiptData
	receiptsData, err = select_UserReceipts(user_id)

	var data global.UserReceiptsData
	data.ReceiptsData = receiptsData

	return data, nil
}


