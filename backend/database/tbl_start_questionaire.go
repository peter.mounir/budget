package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"backend/global"
)

func Create_Start_Questionnaire(tx *sql.Tx, sync_id int64, p *global.StartQuestionnaire) error {
	sqlStatement := "" +
		"INSERT INTO tbl_start_questionnaire (sync_id, results) " +
		"VALUES ($1, $2) "

	_, err = tx.Exec(sqlStatement, sync_id, p.Results)
	if err != nil {
		return err
	}

	global.MyPrint("& Inserted Start_Questionnaire")
	return nil
}

func Delete_Start_Questionnaire(tx *sql.Tx, sync_id int64) error {
	sqlStatement := "" +
		"DELETE FROM tbl_start_questionnaire " +
		"WHERE sync_id=$1 "

	_, err = tx.Exec(sqlStatement, sync_id)
	if err != nil {
		return err
	}

	global.MyPrint("& Deleted Start_Questionnaire")
	return nil
}

func Exists_Start_Questionnaire(sync_id int64) (bool, error) {
	var exists bool

	sqlStatement := "" +
		"SELECT EXISTS " +
		"(SELECT sync_id FROM tbl_start_questionnaire WHERE sync_id=$1) "

	err := con.QueryRow(sqlStatement, sync_id).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}
	return exists, nil
}

func scan_Start_Questionnaire_Row(rows *sql.Rows) global.StartQuestionnaire {
	var sync_id int64
	var results string

	err = rows.Scan(&sync_id, &results)
	if err != nil {
		panic(err)
	}
	return global.StartQuestionnaire{
		Results:             results}
}

func Select_Start_Questionnaire(sync_id int64) (global.StartQuestionnaire, error) {
	var startQuestionnaire global.StartQuestionnaire
	startQuestionnaire.Results = ""

	sqlStatement := "" +
		"SELECT sync_id, results " +
		"FROM tbl_start_questionnaire " +
		"WHERE sync_id=$1 "

	rows, err := con.Query(sqlStatement, sync_id)
	if err != nil {
		return startQuestionnaire, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		startQuestionnaire = scan_Start_Questionnaire_Row(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return startQuestionnaire, err
	}

	if found {
		return startQuestionnaire, nil
	} else {
		return startQuestionnaire, errors.New("Error: Start_Questionnaire not found!")
	}
}

