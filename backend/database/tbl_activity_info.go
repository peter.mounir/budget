package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"backend/global"
)

func Create_Activity_Info(tx *sql.Tx, sync_id int64, p *global.ActivityInfo) error {
	sqlStatement := "" +
		"INSERT INTO tbl_activity_info (sync_id, treeCode, dateId, activityType, nodeCode, " +
		"	description, tick, height, startHour, startMinute, stopHour, stopMinute) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) "

	_, err = tx.Exec(sqlStatement, sync_id,
		p.TreeCode, p.DateId, p.ActivityType, p.NodeCode, p.Description, 
		p.Tick, p.Height, p.StartHour, p.StartMinute, p.StopHour, p.StopMinute )
	if err != nil {
		return err
	}

	global.MyPrint("& Inserted Activity_Info")
	return nil
}

func Delete_Activity_Infos(tx *sql.Tx, sync_id int64) error {
	sqlStatement := "" +
		"DELETE FROM tbl_activity_info " +
		"WHERE sync_id=$1 "

	_, err = tx.Exec(sqlStatement, sync_id)
	if err != nil {
		return err
	}

	global.MyPrint("& Deleted Activity_Info")
	return nil
}

func Exists_Activity_Infos(sync_id int64) (bool, error) {
	var exists bool

	sqlStatement := "" +
		"SELECT EXISTS " +
		"(SELECT sync_id FROM tbl_activity_info WHERE sync_id=$1) "

	err := con.QueryRow(sqlStatement, sync_id).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}
	return exists, nil
}

func scan_Activity_Info_Row(rows *sql.Rows) global.ActivityInfo {	
	var sync_id int64
	var treeCode string
	var dateId int64
	var activityType int
	var nodeCode string
	var description string
	var tick int
	var height int
	var startHour int
	var startMinute int
	var stopHour int
	var stopMinute int

	err = rows.Scan(&sync_id, &treeCode, &dateId, &activityType, &nodeCode, &description, 
		&tick, &height, &startHour, &startMinute, &stopHour, &stopMinute)
	if err != nil {
		panic(err)
	}
	return global.ActivityInfo{
		TreeCode:   	treeCode,
		DateId:         dateId,
		ActivityType: 	activityType,
		NodeCode:       nodeCode,
		Description:    description,
		Tick:     		tick,
		Height:     	height,
		StartHour:     	startHour,
		StartMinute:    startMinute,
		StopHour:     	stopHour,
		StopMinute:     stopMinute}
}

func Select_Activity_Infos(sync_id int64) ([]global.ActivityInfo, error) {
	var activityInfos []global.ActivityInfo

	sqlStatement := "" +
		"SELECT sync_id, treeCode, dateId, activityType, nodeCode, " +
		"	description, tick, height, startHour, startMinute, stopHour, stopMinute " +
		"FROM tbl_activity_info " +
		"WHERE sync_id=$1 "

	rows, err := con.Query(sqlStatement, sync_id)
	if err != nil {
		return activityInfos, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		activityInfos = append(activityInfos, scan_Activity_Info_Row(rows))
		found = true
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return activityInfos, err
	}

	if found {
		return activityInfos, nil
	} else {
		return activityInfos, errors.New("Error: Activity_Infos not found!")
	}
}
