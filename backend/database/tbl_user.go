package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"backend/global"
)

const ENCRYPT bool = true

func Create_User(user global.User) (int64, error) {
	var sqlStatement string 
	if ENCRYPT {
		sqlStatement = "" +
			"INSERT INTO tbl_user (group_id, name, password, sync_order) " +
			"VALUES ($1, $2, crypt($3, gen_salt('bf', 8)), 0) " +
			"RETURNING id "
	} else {
	 	sqlStatement = "" +
	 		"INSERT INTO tbl_user (group_id, name, password, sync_order) " +
	 		"VALUES ($1, $2, $3, 0) " +
	 		"RETURNING id "
	}

	var id int64 = -1

	row := con.QueryRow(sqlStatement, user.Group_Id, user.Name, user.Password)
	switch err = row.Scan(&id); err {
	case nil:
		//global.MyPrint("& Created User")
		return id, nil
	default:
		return id, err
	}
}

func Update_User_Password(user_id int64, password string) error {
	var sqlStatement string 
	if ENCRYPT {
		sqlStatement = "" +
			"UPDATE tbl_user " +
			"SET password=crypt($1, gen_salt('bf', 8)) " +
			"WHERE id=$2 "
	} else {
		sqlStatement = "" +
		 	"UPDATE tbl_user " +
		 	"SET password=$1 " +
		 	"WHERE id=$2 "
	}

	_, err = con.Exec(sqlStatement, password, user_id)
	if err != nil {
		return err
	}

	//global.MyPrint("& Updated User Password")
	return nil
}

func Update_UserSyncOrder(tx *sql.Tx, user global.User) error {
	sqlStatement := "" +
		"UPDATE tbl_user " +
		"SET sync_order=$1 " +
		"WHERE id=$2 "

	_, err = tx.Exec(sqlStatement, user.Sync_Order, user.Id)
	if err != nil {
		return err
	}

	global.MyPrint("& Updated User Update")
	return nil
}

func Change_AllUsersGroup(old_group_id int64, new_group_id int64) error {
	sqlStatement := "" +
		"UPDATE tbl_user " +
		"SET group_id=$1 " +
		"WHERE group_id=$2 "

	_, err = con.Exec(sqlStatement, new_group_id, old_group_id)
	if err != nil {
		return err
	}

	return nil
}

func Change_OneUserGroup(user global.User, new_group_id int64) error {
	sqlStatement := "" +
		"UPDATE tbl_user " +
		"SET group_id=$1 " +
		"WHERE id=$2 "

	_, err = con.Exec(sqlStatement, new_group_id, user.Id)
	if err != nil {
		return err
	}

	return nil
}

func scan_User_Row(rows *sql.Rows) global.User {
	var id int64
	var group_id int64
	var name string
	var password string
	var sync_order int64
	err = rows.Scan(&id, &group_id, &name, &password, &sync_order)
	if err != nil {
		panic(err)
	}
	return global.User{
		Id:         id,
		Group_Id:	group_id,
		Name:       name,
		Password:   "SECRET",
		Sync_Order:		sync_order}
}

func Select_User_ByNamePasswordGroup(user global.User) (global.User, error) {
	var foundUser global.User

	var sqlStatement string 
	if ENCRYPT {
		sqlStatement = "" +
			"SELECT id, group_id, name, password, sync_order " +
			"FROM tbl_user " +
			"WHERE group_id=$1 AND name=$2 AND password=crypt($3, password) LIMIT 1 "
	} else {
		sqlStatement = "" +
			"SELECT id, group_id, name, password, sync_order " +
		 	"FROM tbl_user " +
		 	"WHERE group_id=$1 AND name=$2 AND password=$3 LIMIT 1 "
	}

	rows, err := con.Query(sqlStatement, user.Group_Id, user.Name, user.Password)
	if err != nil {
		return foundUser, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		foundUser = scan_User_Row(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return foundUser, err
	}

	if found {
		return foundUser, nil
	} else {
		return foundUser, errors.New("Error: User-Password combination does not have permission")
	}
}

func Select_User_ByNamePassword(user global.User) (global.User, error) {
	var foundUser global.User

	var sqlStatement string 
	if ENCRYPT {
		sqlStatement = "" +
			"SELECT id, group_id, name, password, sync_order " +
			"FROM tbl_user " +
			"WHERE name=$1 AND password=crypt($2, password) LIMIT 1 "
	} else {
		sqlStatement = "" +
		 	"SELECT id, group_id, name, password, sync_order " +
		 	"FROM tbl_user " +
		 	"WHERE name=$1 AND password=$2 LIMIT 1 "
	}

	rows, err := con.Query(sqlStatement, user.Name, user.Password)
	if err != nil {
		return foundUser, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		foundUser = scan_User_Row(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return foundUser, err
	}

	if found {
		return foundUser, nil
	} else {
		return foundUser, errors.New("Error: User-Password combination does not have permission")
	}
}

func Select_User_ById(id int64) (global.User, error) {
	var user global.User

	sqlStatement := "" +
	    "SELECT id, group_id, name, password, sync_order " +
		"FROM tbl_user " +
		"WHERE id=$1 LIMIT 1 "

	rows, err := con.Query(sqlStatement, id)
	if err != nil {
		return user, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		user = scan_User_Row(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return user, err
	}

	if found {
		return user, nil
	} else {
		return user, errors.New("Error: User not found")
	}
}

func Select_User_ByName(name string) (global.User, error) {
	var user global.User

	sqlStatement := "" +
	    "SELECT id, group_id, name, password, sync_order " +
		"FROM tbl_user " +
		"WHERE name=$1 LIMIT 1 "

	rows, err := con.Query(sqlStatement, name)
	if err != nil {
		return user, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		user = scan_User_Row(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return user, err
	}

	if found {
		return user, nil
	} else {
		return user, errors.New("Error: User not found")
	}
}

func Exists_Users_InGroup(group_id int64) (bool, error) {

	sqlStatement := "" +
		"SELECT id, group_id, name, password, sync_order " +
		"FROM tbl_user " +
		"WHERE group_id=$1 LIMIT 1 "

	rows, err := con.Query(sqlStatement, group_id)
	if err != nil {
		return false, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return false, err
	}

	if found {
		return true, nil
	} else {
		return false, nil
	}
}

func Select_Users_ByGroup(group_id int64) ([]global.User, error) {
	var users []global.User

	sqlStatement := "" +
		"SELECT id, group_id, name, password, sync_order " +
		"FROM tbl_user " +
		"WHERE group_id=$1 "


	rows, err := con.Query(sqlStatement, group_id)
	if err != nil {
		return users, err
	}
	defer rows.Close()

	for rows.Next() {
		users = append(users, scan_User_Row(rows))
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return users, err
	}

	return users, nil
}



func scan_User_Name_Row(rows *sql.Rows) global.UserName {
	var name string
	err = rows.Scan( &name)
	if err != nil {
		panic(err)
	}
	return global.UserName{
		Name:       name}
}

func Select_All_Users() ([]global.UserName, error) {
	var users []global.UserName

	sqlStatement := "" +
		"SELECT name " +
		"FROM tbl_user " 


	rows, err := con.Query(sqlStatement)
	if err != nil {
		return users, err
	}
	defer rows.Close()

	for rows.Next() {
		users = append(users, scan_User_Name_Row(rows))
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return users, err
	}

	return users, nil
}

func Delete_User(name string) error {
	sqlStatement := "" +
		"DELETE FROM tbl_user " +
		"WHERE name=$1 "

	_, err = con.Exec(sqlStatement, name)
	if err != nil {
		return err
	}

	global.MyPrint("& Deleted User")
	return nil
}