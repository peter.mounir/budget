package database

import (
	"errors"
	_ "github.com/lib/pq"
	"backend/global"
)

// ### PHONE ######################################################################################

func RegisterPhone(user global.User, phone global.Phone) (global.Phone, error) {
	phone.Id = -1

	if !IsUserEnabled(user) {
		return phone, errors.New("Error: User is not enabled! " + user.Name + " " + user.Password)
	}

	user, err = Select_User_ByNamePassword(user)
	if err != nil {
		return phone, errors.New("Error: User has no permission!")
	}

	phone.User_Id = user.Id
	phone, err = Select_Phone_ByUserIdName(phone)
	if err != nil {
		return phone, errors.New("Error: Phone does not exist!")
	}

	return phone, nil
}

// ### User ######################################################################################
 
func IsUser(user global.User) bool {
	_, err = Select_User_ByNamePassword(user)
	if err != nil {
		return false
	}
	return true
}

func IsUserSuperuser(user global.User) bool {
	user, err := Select_User_ByNamePassword(user)
	if err != nil {
		return false
	}

	group, err := Select_Group_ById(user.Group_Id)
	if err != nil {
		return false
	}

	return group.Name == global.GROUP_SUPERUSER
}

func IsUserMonitoring(user global.User) bool {
	user, err := Select_User_ByNamePassword(user)
	if err != nil {
		return false
	}

	group, err := Select_Group_ById(user.Group_Id)
	if err != nil {
		return false
	}

	return group.Name == global.GROUP_MONITORING
}

func IsUserCasemanagement(user global.User) bool {
	user, err := Select_User_ByNamePassword(user)
	if err != nil {
		return false
	}

	group, err := Select_Group_ById(user.Group_Id)
	if err != nil {
		return false
	}

	return group.Name == global.GROUP_CMM
}

func IsUserCbsProcess(user global.User) bool {
	user, err := Select_User_ByNamePassword(user)
	if err != nil {
		return false
	}

	group, err := Select_Group_ById(user.Group_Id)
	if err != nil {
		return false
	}

	return group.Name == global.GROUP_CBS_PROCESS
}

func IsUserEnabled(user global.User) bool {
	user, err := Select_User_ByNamePassword(user)
	if err != nil {
		return false
	}

	group, err := Select_Group_ById(user.Group_Id)
	if err != nil {
		return false
	}

	return group.Status == global.STATUS_ENABLED
}
