import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/color_pallet.dart';
import '../model/international.dart';
import '../state/translations.dart';
import 'user_progress.dart';

Future<void> configureApp() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: ColorPallet.primaryColor,
    statusBarBrightness: Brightness.dark,
  ));
  final country = International.countryFromCode(ui.window.locale.languageCode);
  final prefs = await SharedPreferences.getInstance();
  LanguageSetting.key = prefs.getString('languagePreference') ?? country.languageId;
  LanguageSetting.tablePreference = prefs.getString('tablePreference') ?? country.id;
  await initializeTranslations();
}

Future<bool> isInitialized() async {
  return UserProgressController().isInitialized();
}
