// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_start_questionnaire_body.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncStartQuestionnaireBody> _$syncStartQuestionnaireBodySerializer =
    new _$SyncStartQuestionnaireBodySerializer();

class _$SyncStartQuestionnaireBodySerializer
    implements StructuredSerializer<SyncStartQuestionnaireBody> {
  @override
  final Iterable<Type> types = const [
    SyncStartQuestionnaireBody,
    _$SyncStartQuestionnaireBody
  ];
  @override
  final String wireName = 'SyncStartQuestionnaireBody';

  @override
  Iterable<Object> serialize(
      Serializers serializers, SyncStartQuestionnaireBody object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'user',
      serializers.serialize(object.user,
          specifiedType: const FullType(SyncUser)),
      'phone',
      serializers.serialize(object.phone,
          specifiedType: const FullType(SyncPhone)),
      'syncOrder',
      serializers.serialize(object.syncOrder,
          specifiedType: const FullType(int)),
      'synchronisation',
      serializers.serialize(object.synchronisation,
          specifiedType: const FullType(SyncSync)),
      'startQuestionnaire',
      serializers.serialize(object.startQuestionnaire,
          specifiedType: const FullType(SyncStartQuestionnaire)),
    ];

    return result;
  }

  @override
  SyncStartQuestionnaireBody deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncStartQuestionnaireBodyBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncUser)) as SyncUser);
          break;
        case 'phone':
          result.phone.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncPhone)) as SyncPhone);
          break;
        case 'syncOrder':
          result.syncOrder = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'synchronisation':
          result.synchronisation.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncSync)) as SyncSync);
          break;
        case 'startQuestionnaire':
          result.startQuestionnaire.replace(serializers.deserialize(value,
                  specifiedType: const FullType(SyncStartQuestionnaire))
              as SyncStartQuestionnaire);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncStartQuestionnaireBody extends SyncStartQuestionnaireBody {
  @override
  final SyncUser user;
  @override
  final SyncPhone phone;
  @override
  final int syncOrder;
  @override
  final SyncSync synchronisation;
  @override
  final SyncStartQuestionnaire startQuestionnaire;

  factory _$SyncStartQuestionnaireBody(
          [void Function(SyncStartQuestionnaireBodyBuilder) updates]) =>
      (new SyncStartQuestionnaireBodyBuilder()..update(updates)).build();

  _$SyncStartQuestionnaireBody._(
      {this.user,
      this.phone,
      this.syncOrder,
      this.synchronisation,
      this.startQuestionnaire})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        user, 'SyncStartQuestionnaireBody', 'user');
    BuiltValueNullFieldError.checkNotNull(
        phone, 'SyncStartQuestionnaireBody', 'phone');
    BuiltValueNullFieldError.checkNotNull(
        syncOrder, 'SyncStartQuestionnaireBody', 'syncOrder');
    BuiltValueNullFieldError.checkNotNull(
        synchronisation, 'SyncStartQuestionnaireBody', 'synchronisation');
    BuiltValueNullFieldError.checkNotNull(
        startQuestionnaire, 'SyncStartQuestionnaireBody', 'startQuestionnaire');
  }

  @override
  SyncStartQuestionnaireBody rebuild(
          void Function(SyncStartQuestionnaireBodyBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncStartQuestionnaireBodyBuilder toBuilder() =>
      new SyncStartQuestionnaireBodyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncStartQuestionnaireBody &&
        user == other.user &&
        phone == other.phone &&
        syncOrder == other.syncOrder &&
        synchronisation == other.synchronisation &&
        startQuestionnaire == other.startQuestionnaire;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, user.hashCode), phone.hashCode), syncOrder.hashCode),
            synchronisation.hashCode),
        startQuestionnaire.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncStartQuestionnaireBody')
          ..add('user', user)
          ..add('phone', phone)
          ..add('syncOrder', syncOrder)
          ..add('synchronisation', synchronisation)
          ..add('startQuestionnaire', startQuestionnaire))
        .toString();
  }
}

class SyncStartQuestionnaireBodyBuilder
    implements
        Builder<SyncStartQuestionnaireBody, SyncStartQuestionnaireBodyBuilder> {
  _$SyncStartQuestionnaireBody _$v;

  SyncUserBuilder _user;
  SyncUserBuilder get user => _$this._user ??= new SyncUserBuilder();
  set user(SyncUserBuilder user) => _$this._user = user;

  SyncPhoneBuilder _phone;
  SyncPhoneBuilder get phone => _$this._phone ??= new SyncPhoneBuilder();
  set phone(SyncPhoneBuilder phone) => _$this._phone = phone;

  int _syncOrder;
  int get syncOrder => _$this._syncOrder;
  set syncOrder(int syncOrder) => _$this._syncOrder = syncOrder;

  SyncSyncBuilder _synchronisation;
  SyncSyncBuilder get synchronisation =>
      _$this._synchronisation ??= new SyncSyncBuilder();
  set synchronisation(SyncSyncBuilder synchronisation) =>
      _$this._synchronisation = synchronisation;

  SyncStartQuestionnaireBuilder _startQuestionnaire;
  SyncStartQuestionnaireBuilder get startQuestionnaire =>
      _$this._startQuestionnaire ??= new SyncStartQuestionnaireBuilder();
  set startQuestionnaire(SyncStartQuestionnaireBuilder startQuestionnaire) =>
      _$this._startQuestionnaire = startQuestionnaire;

  SyncStartQuestionnaireBodyBuilder();

  SyncStartQuestionnaireBodyBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _user = $v.user.toBuilder();
      _phone = $v.phone.toBuilder();
      _syncOrder = $v.syncOrder;
      _synchronisation = $v.synchronisation.toBuilder();
      _startQuestionnaire = $v.startQuestionnaire.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncStartQuestionnaireBody other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncStartQuestionnaireBody;
  }

  @override
  void update(void Function(SyncStartQuestionnaireBodyBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncStartQuestionnaireBody build() {
    _$SyncStartQuestionnaireBody _$result;
    try {
      _$result = _$v ??
          new _$SyncStartQuestionnaireBody._(
              user: user.build(),
              phone: phone.build(),
              syncOrder: BuiltValueNullFieldError.checkNotNull(
                  syncOrder, 'SyncStartQuestionnaireBody', 'syncOrder'),
              synchronisation: synchronisation.build(),
              startQuestionnaire: startQuestionnaire.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        user.build();
        _$failedField = 'phone';
        phone.build();

        _$failedField = 'synchronisation';
        synchronisation.build();
        _$failedField = 'startQuestionnaire';
        startQuestionnaire.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SyncStartQuestionnaireBody', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
