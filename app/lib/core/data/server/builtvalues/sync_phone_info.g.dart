// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_phone_info.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncPhoneInfo> _$syncPhoneInfoSerializer =
    new _$SyncPhoneInfoSerializer();

class _$SyncPhoneInfoSerializer implements StructuredSerializer<SyncPhoneInfo> {
  @override
  final Iterable<Type> types = const [SyncPhoneInfo, _$SyncPhoneInfo];
  @override
  final String wireName = 'SyncPhoneInfo';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncPhoneInfo object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'phoneId',
      serializers.serialize(object.phoneId, specifiedType: const FullType(int)),
      'key',
      serializers.serialize(object.key, specifiedType: const FullType(String)),
      'value',
      serializers.serialize(object.value,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SyncPhoneInfo deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncPhoneInfoBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'phoneId':
          result.phoneId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'key':
          result.key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncPhoneInfo extends SyncPhoneInfo {
  @override
  final int id;
  @override
  final int phoneId;
  @override
  final String key;
  @override
  final String value;

  factory _$SyncPhoneInfo([void Function(SyncPhoneInfoBuilder) updates]) =>
      (new SyncPhoneInfoBuilder()..update(updates)).build();

  _$SyncPhoneInfo._({this.id, this.phoneId, this.key, this.value}) : super._() {
    BuiltValueNullFieldError.checkNotNull(id, 'SyncPhoneInfo', 'id');
    BuiltValueNullFieldError.checkNotNull(phoneId, 'SyncPhoneInfo', 'phoneId');
    BuiltValueNullFieldError.checkNotNull(key, 'SyncPhoneInfo', 'key');
    BuiltValueNullFieldError.checkNotNull(value, 'SyncPhoneInfo', 'value');
  }

  @override
  SyncPhoneInfo rebuild(void Function(SyncPhoneInfoBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncPhoneInfoBuilder toBuilder() => new SyncPhoneInfoBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncPhoneInfo &&
        id == other.id &&
        phoneId == other.phoneId &&
        key == other.key &&
        value == other.value;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), phoneId.hashCode), key.hashCode),
        value.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncPhoneInfo')
          ..add('id', id)
          ..add('phoneId', phoneId)
          ..add('key', key)
          ..add('value', value))
        .toString();
  }
}

class SyncPhoneInfoBuilder
    implements Builder<SyncPhoneInfo, SyncPhoneInfoBuilder> {
  _$SyncPhoneInfo _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  int _phoneId;
  int get phoneId => _$this._phoneId;
  set phoneId(int phoneId) => _$this._phoneId = phoneId;

  String _key;
  String get key => _$this._key;
  set key(String key) => _$this._key = key;

  String _value;
  String get value => _$this._value;
  set value(String value) => _$this._value = value;

  SyncPhoneInfoBuilder();

  SyncPhoneInfoBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _phoneId = $v.phoneId;
      _key = $v.key;
      _value = $v.value;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncPhoneInfo other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncPhoneInfo;
  }

  @override
  void update(void Function(SyncPhoneInfoBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncPhoneInfo build() {
    final _$result = _$v ??
        new _$SyncPhoneInfo._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, 'SyncPhoneInfo', 'id'),
            phoneId: BuiltValueNullFieldError.checkNotNull(
                phoneId, 'SyncPhoneInfo', 'phoneId'),
            key: BuiltValueNullFieldError.checkNotNull(
                key, 'SyncPhoneInfo', 'key'),
            value: BuiltValueNullFieldError.checkNotNull(
                value, 'SyncPhoneInfo', 'value'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
