import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import '../../../../features/para_data/para_data.dart';
import 'serializers.dart';
import 'sync_sync.dart';

part 'sync_paradata_data.g.dart';

abstract class SyncParadataData implements Built<SyncParadataData, SyncParadataDataBuilder> {
  static Serializer<SyncParadataData> get serializer => _$syncParadataDataSerializer;

  SyncSync get synchronisation;
  BuiltList<ParaData> get paradatas;

  factory SyncParadataData([Function(SyncParadataDataBuilder b) updates]) = _$SyncParadataData;

  SyncParadataData._();

  factory SyncParadataData.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
