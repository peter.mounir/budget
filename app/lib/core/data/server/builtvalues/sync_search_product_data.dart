import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

import 'serializers.dart';
import 'sync_search_product.dart';
import 'sync_sync.dart';

part 'sync_search_product_data.g.dart';

abstract class SyncSearchProductData implements Built<SyncSearchProductData, SyncSearchProductDataBuilder> {
  static Serializer<SyncSearchProductData> get serializer => _$syncSearchProductDataSerializer;

  SyncSync get synchronisation;
  SyncSearchProduct get searchProduct;

  factory SyncSearchProductData([Function(SyncSearchProductDataBuilder b) updates]) = _$SyncSearchProductData;

  SyncSearchProductData._();

  factory SyncSearchProductData.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }
}
