// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(ParaData.serializer)
      ..add(SyncGroupInfo.serializer)
      ..add(SyncId.serializer)
      ..add(SyncImage.serializer)
      ..add(SyncParadataBody.serializer)
      ..add(SyncParadataData.serializer)
      ..add(SyncPhone.serializer)
      ..add(SyncPhoneInfo.serializer)
      ..add(SyncProduct.serializer)
      ..add(SyncPullBody.serializer)
      ..add(SyncPushReceiptBody.serializer)
      ..add(SyncPushSearchProductBody.serializer)
      ..add(SyncPushSearchStoreBody.serializer)
      ..add(SyncReceiptData.serializer)
      ..add(SyncRegisterBody.serializer)
      ..add(SyncRegisterData.serializer)
      ..add(SyncSearchProduct.serializer)
      ..add(SyncSearchProductData.serializer)
      ..add(SyncSearchStore.serializer)
      ..add(SyncSearchStoreData.serializer)
      ..add(SyncStartQuestionnaire.serializer)
      ..add(SyncStartQuestionnaireBody.serializer)
      ..add(SyncStartQuestionnaireData.serializer)
      ..add(SyncSync.serializer)
      ..add(SyncTransaction.serializer)
      ..add(SyncUser.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ParaData)]),
          () => new ListBuilder<ParaData>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ParaData)]),
          () => new ListBuilder<ParaData>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SyncGroupInfo)]),
          () => new ListBuilder<SyncGroupInfo>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SyncPhoneInfo)]),
          () => new ListBuilder<SyncPhoneInfo>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SyncProduct)]),
          () => new ListBuilder<SyncProduct>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SyncProduct)]),
          () => new ListBuilder<SyncProduct>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
