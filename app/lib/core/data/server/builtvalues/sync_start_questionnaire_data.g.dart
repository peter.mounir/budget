// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_start_questionnaire_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncStartQuestionnaireData> _$syncStartQuestionnaireDataSerializer =
    new _$SyncStartQuestionnaireDataSerializer();

class _$SyncStartQuestionnaireDataSerializer
    implements StructuredSerializer<SyncStartQuestionnaireData> {
  @override
  final Iterable<Type> types = const [
    SyncStartQuestionnaireData,
    _$SyncStartQuestionnaireData
  ];
  @override
  final String wireName = 'SyncStartQuestionnaireData';

  @override
  Iterable<Object> serialize(
      Serializers serializers, SyncStartQuestionnaireData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'synchronisation',
      serializers.serialize(object.synchronisation,
          specifiedType: const FullType(SyncSync)),
      'syncStartQuestionnaire',
      serializers.serialize(object.syncStartQuestionnaire,
          specifiedType: const FullType(SyncStartQuestionnaire)),
    ];

    return result;
  }

  @override
  SyncStartQuestionnaireData deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncStartQuestionnaireDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object value = iterator.current;
      switch (key) {
        case 'synchronisation':
          result.synchronisation.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncSync)) as SyncSync);
          break;
        case 'syncStartQuestionnaire':
          result.syncStartQuestionnaire.replace(serializers.deserialize(value,
                  specifiedType: const FullType(SyncStartQuestionnaire))
              as SyncStartQuestionnaire);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncStartQuestionnaireData extends SyncStartQuestionnaireData {
  @override
  final SyncSync synchronisation;
  @override
  final SyncStartQuestionnaire syncStartQuestionnaire;

  factory _$SyncStartQuestionnaireData(
          [void Function(SyncStartQuestionnaireDataBuilder) updates]) =>
      (new SyncStartQuestionnaireDataBuilder()..update(updates)).build();

  _$SyncStartQuestionnaireData._(
      {this.synchronisation, this.syncStartQuestionnaire})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        synchronisation, 'SyncStartQuestionnaireData', 'synchronisation');
    BuiltValueNullFieldError.checkNotNull(syncStartQuestionnaire,
        'SyncStartQuestionnaireData', 'syncStartQuestionnaire');
  }

  @override
  SyncStartQuestionnaireData rebuild(
          void Function(SyncStartQuestionnaireDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncStartQuestionnaireDataBuilder toBuilder() =>
      new SyncStartQuestionnaireDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncStartQuestionnaireData &&
        synchronisation == other.synchronisation &&
        syncStartQuestionnaire == other.syncStartQuestionnaire;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc(0, synchronisation.hashCode), syncStartQuestionnaire.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncStartQuestionnaireData')
          ..add('synchronisation', synchronisation)
          ..add('syncStartQuestionnaire', syncStartQuestionnaire))
        .toString();
  }
}

class SyncStartQuestionnaireDataBuilder
    implements
        Builder<SyncStartQuestionnaireData, SyncStartQuestionnaireDataBuilder> {
  _$SyncStartQuestionnaireData _$v;

  SyncSyncBuilder _synchronisation;
  SyncSyncBuilder get synchronisation =>
      _$this._synchronisation ??= new SyncSyncBuilder();
  set synchronisation(SyncSyncBuilder synchronisation) =>
      _$this._synchronisation = synchronisation;

  SyncStartQuestionnaireBuilder _syncStartQuestionnaire;
  SyncStartQuestionnaireBuilder get syncStartQuestionnaire =>
      _$this._syncStartQuestionnaire ??= new SyncStartQuestionnaireBuilder();
  set syncStartQuestionnaire(
          SyncStartQuestionnaireBuilder syncStartQuestionnaire) =>
      _$this._syncStartQuestionnaire = syncStartQuestionnaire;

  SyncStartQuestionnaireDataBuilder();

  SyncStartQuestionnaireDataBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _synchronisation = $v.synchronisation.toBuilder();
      _syncStartQuestionnaire = $v.syncStartQuestionnaire.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncStartQuestionnaireData other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SyncStartQuestionnaireData;
  }

  @override
  void update(void Function(SyncStartQuestionnaireDataBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncStartQuestionnaireData build() {
    _$SyncStartQuestionnaireData _$result;
    try {
      _$result = _$v ??
          new _$SyncStartQuestionnaireData._(
              synchronisation: synchronisation.build(),
              syncStartQuestionnaire: syncStartQuestionnaire.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'synchronisation';
        synchronisation.build();
        _$failedField = 'syncStartQuestionnaire';
        syncStartQuestionnaire.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SyncStartQuestionnaireData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
