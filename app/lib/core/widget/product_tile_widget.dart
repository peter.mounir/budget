import 'package:flutter/material.dart';

import '../controller/util/currency_formatter.dart';
import '../controller/util/responsive_ui.dart';
import '../model/color_pallet.dart';
import '../model/receipt_product.dart';

class ProductTileWidget extends StatelessWidget {
  final ReceiptProduct product;

  const ProductTileWidget(this.product);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 8.0 * y, bottom: 8.0 * y, left: 0 * x),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 70 * x,
            child: Column(children: <Widget>[
              Text(
                '${product.count}x',
                style: TextStyle(fontSize: 14 * f, color: ColorPallet.darkTextColor, fontWeight: FontWeight.w600),
              ),
              Text(
                product.getPrice().toStringAsFixed(2).addCurrencyFormat(),
                style: TextStyle(fontSize: 14 * f, color: ColorPallet.darkTextColor, fontWeight: FontWeight.w600),
              ),
            ]),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                width: 225 * x,
                child: Text(
                  product.name,
                  style: TextStyle(fontSize: 14 * f, color: ColorPallet.darkTextColor, fontWeight: FontWeight.w600),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              SizedBox(height: 5 * y),
              SizedBox(
                width: 225 * x,
                child: Text(
                  product.category,
                  style: TextStyle(fontSize: 14 * f, color: ColorPallet.midGray, fontWeight: FontWeight.w600),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      product.getPrice().toStringAsFixed(2).addCurrencyFormat(),
                      style: TextStyle(fontSize: 14 * f, color: ColorPallet.pink, fontWeight: FontWeight.w600),
                    ),
                    SizedBox(height: 5 * y),
                  ],
                ),
                SizedBox(width: 30 * x),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
