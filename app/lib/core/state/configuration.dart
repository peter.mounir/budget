import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../controller/user_progress.dart';

enum InsightsConfiguration { enabled, delayed }
enum QuestionnaireConfiguration { enabled, disabled }
enum ParadataConfiguration { enabled, disabled }
enum OCRConfiguration { local_ocr, only_crop, disabled }

InsightsConfiguration insightsDefault = InsightsConfiguration.enabled;
QuestionnaireConfiguration questionnaireDefault = QuestionnaireConfiguration.enabled;
ParadataConfiguration paradataDefault = ParadataConfiguration.enabled;
OCRConfiguration ocrDefault = OCRConfiguration.local_ocr;

int insightsDelayInHours = 14 * 24;

class Configuration extends Model {
  bool _loaded = false;
  InsightsConfiguration insights;
  QuestionnaireConfiguration questionnaire;
  ParadataConfiguration paradata;
  OCRConfiguration ocr;

  Configuration() {
    if (_loaded == false) {
      load();
      _loaded = true;
    }
  }

  Future<void> load() async {
    final _preferences = await SharedPreferences.getInstance();

    insights = EnumToString.fromString(InsightsConfiguration.values, _preferences.getString('InsightsConfiguration') ?? '') ?? insightsDefault;
    questionnaire =
        EnumToString.fromString(QuestionnaireConfiguration.values, _preferences.getString('QuestionnaireConfiguration') ?? '') ?? questionnaireDefault;
    paradata = EnumToString.fromString(ParadataConfiguration.values, _preferences.getString('ParadataConfiguration') ?? '') ?? paradataDefault;
    ocr = EnumToString.fromString(OCRConfiguration.values, _preferences.getString('OCRConfiguration') ?? '') ?? ocrDefault;

    final progressLists = await UserProgressController().getProgressLists();
    print(progressLists.daysRemaining.length);
    if (progressLists.daysRemaining.isEmpty) {
      insights = InsightsConfiguration.enabled;
    }
    notifyListeners();
  }

  Future<void> save({String insightsString, String questionnaireString, String paradataString, String ocrString}) async {
    final _preferences = await SharedPreferences.getInstance();

    insights = insightsString.toEnum(InsightsConfiguration.values, insightsDefault);
    questionnaire = questionnaireString.toEnum(QuestionnaireConfiguration.values, questionnaireDefault);
    paradata = paradataString.toEnum(ParadataConfiguration.values, paradataDefault);
    ocr = ocrString.toEnum(OCRConfiguration.values, ocrDefault);

    await _preferences.setString('InsightsConfiguration', EnumToString.convertToString(insights));
    await _preferences.setString('QuestionnaireConfiguration', EnumToString.convertToString(questionnaire));
    await _preferences.setString('ParadataConfiguration', EnumToString.convertToString(paradata));
    await _preferences.setString('OCRConfiguration', EnumToString.convertToString(ocr));

    notifyListeners();
  }

  static Configuration of(BuildContext context) => ScopedModel.of<Configuration>(context);
}

extension ToEnum on String {
  T toEnum<T>(List<T> enumValues, T defaultEnum) {
    if (this == null) {
      return defaultEnum;
    }
    final enumValue = EnumToString.fromString(enumValues, this);
    assert(enumValue != null);
    return enumValue ?? defaultEnum;
  }
}
