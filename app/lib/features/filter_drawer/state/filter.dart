import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class FilterState extends Model {
  GlobalKey<ScaffoldState> scaffoldKey;
  bool isSearchMode = false;
  String searchInput;
  DateTime startDate, endDate;
  double minValue, maxValue;

  FilterState(this.scaffoldKey);

  void reset() {
    searchInput = null;
    minValue = null;
    maxValue = null;
    startDate = null;
    endDate = null;
    isSearchMode = false;
    notifyListeners();
  }

  bool isOff() {
    return minValue == null && maxValue == null && startDate == null && endDate == null;
  }

  void setSearchInput(String searchInput) {
    this.searchInput = searchInput;
    notifyListeners();
  }

  void enableSearchMode() {
    isSearchMode = true;
    notifyListeners();
  }

  void disableSearchMode() {
    isSearchMode = false;
    reset();
    notifyListeners();
  }

  void showDrawer() {
    scaffoldKey.currentState.openEndDrawer();
  }

  void notify() {
    notifyListeners();
  }

  static FilterState of(BuildContext context) => ScopedModel.of<FilterState>(context);
}
