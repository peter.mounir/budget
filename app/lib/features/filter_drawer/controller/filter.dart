import '../../../core/data/database/tables/receipt.dart';
import '../../../core/model/date_range.dart';
import '../../../core/model/value_range.dart';

class FilterController {
  Future<DateRange> getDateRange() async {
    final receipts = await ReceiptTable().query();
    final dateRange = DateRange.empty();
    if (receipts.isEmpty) {
      dateRange.start = DateTime.now();
      dateRange.end = DateTime.now().add(const Duration(days: 31));
    } else {
      dateRange.start = receipts.last.date;
      dateRange.end = receipts.first.date;
    }
    return dateRange;
  }

  Future<ValueRange> getValueRange() async {
    final receipts = await ReceiptTable().query();
    final valueRange = ValueRange.empty();
    if (receipts.isEmpty) {
      valueRange.min = 0.0;
      valueRange.max = 0.0;
    } else {
      valueRange.min = double.infinity;
      valueRange.max = 0.0;
      for (final receipt in receipts) {
        final receiptPrice = receipt.products.getTotalPrice();
        valueRange.min = receiptPrice < valueRange.min ? receiptPrice : valueRange.min;
        valueRange.max = receiptPrice > valueRange.max ? receiptPrice : valueRange.max;
      }
    }
    return valueRange;
  }
}
