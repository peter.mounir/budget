import 'package:flutter/material.dart';
import 'package:questionnaire/questionnaire.dart';

import '../../core/model/color_pallet.dart';
import '../../core/state/translations.dart';

mixin CreateQuestionnaireMixin {
  List<QuestionBase> questionList;
  Map<String, Color> colorList;
  Map<String, String> textList;
  Translations translations;
  final String pageKey = 'Questionnaire';

  String _parseTextAndFillStartAndEndDate(String parseString, String startDate, String endDate) {
    return parseString.replaceAll('\$startDate', startDate).replaceAll('\$endDate', endDate);
  }

  void _createQuestionListSlovenia(String startDate, String endDate) {
    // question 1
    questionList = List<QuestionBase>.empty(growable: true);
    final _conditions = <String, int>{};
    final ageQuestion = NormalQuestionDefinition(
      questionNumber: 1,
      questionText: Translations.textStatic('question01Text', pageKey),
      hintText: Translations.textStatic('question01HintText', pageKey),
      keyboardType: TextInputType.number,
      numberValidationText: Translations.textStatic('question01ValidationText', pageKey),
      conditions: _conditions,
    );

    // question 4
    final woningChoices = List<ChoiceDefinition>.empty(growable: true);
    woningChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question04Choice01Text', pageKey)));
    woningChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question04Choice02Text', pageKey)));
    woningChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question04Choice03Text', pageKey)));
    woningChoices.add(ChoiceDefinition(
      choiceNumber: 4,
      choiceText: Translations.textStatic('question04Choice04Text', pageKey),
    ));
    woningChoices.add(ChoiceDefinition(
      choiceNumber: 5,
      choiceText: Translations.textStatic('question04Choice05Text', pageKey),
    ));
    final woningQuestion =
        SingleChoiceQuestionDefinition(questionNumber: 4, questionText: Translations.textStatic('question04Text', pageKey), choiceList: woningChoices);

    // question 5
    final woningSoortChoices = List<ChoiceDefinition>.empty(growable: true);
    woningSoortChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question05Choice01Text', pageKey)));
    woningSoortChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question05Choice02Text', pageKey)));
    woningSoortChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question05Choice03Text', pageKey)));
    woningSoortChoices.add(ChoiceDefinition(
      choiceNumber: 4,
      choiceText: Translations.textStatic('question05Choice04Text', pageKey),
    ));
    woningSoortChoices.add(ChoiceDefinition(
      choiceNumber: 5,
      choiceText: Translations.textStatic('question05Choice05Text', pageKey),
    ));
    woningSoortChoices.add(ChoiceDefinition(
      choiceNumber: 6,
      choiceText: Translations.textStatic('question05Choice06Text', pageKey),
    ));
    woningSoortChoices.add(ChoiceDefinition(
      choiceNumber: 7,
      choiceText: Translations.textStatic('question05Choice07Text', pageKey),
    ));
    woningSoortChoices.add(ChoiceDefinition(
      choiceNumber: 8,
      choiceText: Translations.textStatic('question05Choice08Text', pageKey),
    ));

    final woningSoortQuestion =
        SingleChoiceQuestionDefinition(questionNumber: 5, questionText: Translations.textStatic('question05Text', pageKey), choiceList: woningSoortChoices);

    // question 6
    final woningGedeelteChoices = List<ChoiceDefinition>.empty(growable: true);
    woningGedeelteChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question06Choice01Text', pageKey)));
    woningGedeelteChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question06Choice02Text', pageKey)));

    final woningGedeelteQuestion =
        SingleChoiceQuestionDefinition(questionNumber: 6, questionText: Translations.textStatic('question06Text', pageKey), choiceList: woningGedeelteChoices);

    // question 7
    final woningPeriodeChoices = List<ChoiceDefinition>.empty(growable: true);
    woningPeriodeChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question07Choice01Text', pageKey)));
    woningPeriodeChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question07Choice02Text', pageKey)));
    woningPeriodeChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question07Choice03Text', pageKey)));

    final woningPeriodeQuestion =
        SingleChoiceQuestionDefinition(questionNumber: 7, questionText: Translations.textStatic('question07Text', pageKey), choiceList: woningPeriodeChoices);

    // question 13
    final vervoerChoices = List<ChoiceDefinition>.empty(growable: true);
    vervoerChoices.add(ChoiceDefinition(
      choiceNumber: 1,
      choiceText: Translations.textStatic('question13Choice01Text', pageKey),
    ));
    vervoerChoices.add(ChoiceDefinition(
      choiceNumber: 2,
      choiceText: Translations.textStatic('question13Choice02Text', pageKey),
    ));
    vervoerChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question13Choice03Text', pageKey)));
    vervoerChoices.add(ChoiceDefinition(
      choiceNumber: 4,
      choiceText: Translations.textStatic('question13Choice04Text', pageKey),
    ));
    vervoerChoices.add(ChoiceDefinition(choiceNumber: 5, choiceText: Translations.textStatic('question13Choice05Text', pageKey), resetOtherFields: true));
    final vervoerQuestion = MultipleChoiceQuestionDefinition(
        questionNumber: 13,
        questionText: Translations.textStatic('question13Text', pageKey),
        hintText: Translations.textStatic('question13HintText', pageKey),
        choiceList: vervoerChoices);

    questionList.add(ageQuestion);
    questionList.add(woningQuestion);
    questionList.add(woningSoortQuestion);
    questionList.add(woningGedeelteQuestion);
    questionList.add(woningPeriodeQuestion);
    questionList.add(vervoerQuestion);
  }

  void _createQuestionList(String language, String startDate, String endDate) {
    // question 1
    questionList = List<QuestionBase>.empty(growable: true);
    final _conditions = <String, int>{'1': 3};
    final ageQuestion = NormalQuestionDefinition(
      questionNumber: 1,
      questionText: Translations.textStatic('question01Text', pageKey),
      hintText: Translations.textStatic('question01HintText', pageKey),
      keyboardType: TextInputType.number,
      numberValidationText: Translations.textStatic('question01ValidationText', pageKey),
      conditions: _conditions,
    );

    // question 2
    final livingSituationChoices = List<ChoiceDefinition>.empty(growable: true);
    livingSituationChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question02Choice01Text', pageKey)));
    livingSituationChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question02Choice02Text', pageKey)));
    livingSituationChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question02Choice03Text', pageKey)));
    livingSituationChoices.add(ChoiceDefinition(choiceNumber: 4, choiceText: Translations.textStatic('question02Choice04Text', pageKey)));
    livingSituationChoices.add(ChoiceDefinition(choiceNumber: 5, choiceText: Translations.textStatic('question02Choice05Text', pageKey)));
    final livingSituationQuestion =
        SingleChoiceQuestionDefinition(questionNumber: 2, questionText: Translations.textStatic('question02Text', pageKey), choiceList: livingSituationChoices);

    // question 3
    final relationMap = <int, List<int>>{};
    relationMap[2] = [2, 4, 5];
    final relationChoices = List<ChoiceDefinition>.empty(growable: true);
    relationChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question03Choice01Text', pageKey)));
    relationChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question03Choice02Text', pageKey)));
    relationChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question03Choice03Text', pageKey)));
    relationChoices.add(ChoiceDefinition(choiceNumber: 4, choiceText: Translations.textStatic('question03Choice04Text', pageKey)));
    relationChoices.add(ChoiceDefinition(choiceNumber: 5, choiceText: Translations.textStatic('question03Choice05Text', pageKey)));
    relationChoices.add(ChoiceDefinition(choiceNumber: 6, choiceText: Translations.textStatic('question03Choice06Text', pageKey)));
    final genderChoices = List<ChoiceDefinition>.empty(growable: true);
    genderChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('questionGenderChoice01Text', pageKey)));
    genderChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('questionGenderChoice02Text', pageKey)));

    final personInfoQuestion = PersonInfoQuestionDefinition(
        questionNumber: 3,
        questionText: Translations.textStatic('question03Text', pageKey),
        questionText2: Translations.textStatic('question03Text2', pageKey),
        questionText3: Translations.textStatic('question03Text3', pageKey),
        questionNumberPersons: 1,
        titleText: Translations.textStatic('question03TitleText', pageKey),
        relationMap: relationMap,
        relationChoices: relationChoices,
        genderChoices: genderChoices);

    // question 4
    final woningChoices = List<ChoiceDefinition>.empty(growable: true);
    woningChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question04Choice01Text', pageKey)));
    woningChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question04Choice02Text', pageKey)));
    woningChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question04Choice03Text', pageKey)));
    woningChoices.add(ChoiceDefinition(
      choiceNumber: 4,
      choiceText: Translations.textStatic('question04Choice04Text', pageKey),
    ));
    woningChoices.add(ChoiceDefinition(
      choiceNumber: 5,
      choiceText: Translations.textStatic('question04Choice05Text', pageKey),
    ));
    final woningQuestion =
        SingleChoiceQuestionDefinition(questionNumber: 4, questionText: Translations.textStatic('question04Text', pageKey), choiceList: woningChoices);

    // question 5
    final woningSoortChoices = List<ChoiceDefinition>.empty(growable: true);
    woningSoortChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question05Choice01Text', pageKey)));
    woningSoortChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question05Choice02Text', pageKey)));
    woningSoortChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question05Choice03Text', pageKey)));
    woningSoortChoices.add(ChoiceDefinition(
      choiceNumber: 4,
      choiceText: Translations.textStatic('question05Choice04Text', pageKey),
    ));
    woningSoortChoices.add(ChoiceDefinition(
      choiceNumber: 5,
      choiceText: Translations.textStatic('question05Choice05Text', pageKey),
    ));
    woningSoortChoices.add(ChoiceDefinition(
      choiceNumber: 6,
      choiceText: Translations.textStatic('question05Choice06Text', pageKey),
    ));
    woningSoortChoices.add(ChoiceDefinition(
      choiceNumber: 7,
      choiceText: Translations.textStatic('question05Choice07Text', pageKey),
    ));
    woningSoortChoices.add(ChoiceDefinition(
      choiceNumber: 8,
      choiceText: Translations.textStatic('question05Choice08Text', pageKey),
    ));

    final woningSoortQuestion =
        SingleChoiceQuestionDefinition(questionNumber: 5, questionText: Translations.textStatic('question05Text', pageKey), choiceList: woningSoortChoices);

    // question 6
    final woningGedeelteChoices = List<ChoiceDefinition>.empty(growable: true);
    woningGedeelteChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question06Choice01Text', pageKey)));
    woningGedeelteChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question06Choice02Text', pageKey)));

    final woningGedeelteQuestion =
        SingleChoiceQuestionDefinition(questionNumber: 6, questionText: Translations.textStatic('question06Text', pageKey), choiceList: woningGedeelteChoices);

    // question 7
    final woningPeriodeChoices = List<ChoiceDefinition>.empty(growable: true);
    woningPeriodeChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question07Choice01Text', pageKey)));
    woningPeriodeChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question07Choice02Text', pageKey)));
    woningPeriodeChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question07Choice03Text', pageKey)));

    final woningPeriodeQuestion =
        SingleChoiceQuestionDefinition(questionNumber: 7, questionText: Translations.textStatic('question07Text', pageKey), choiceList: woningPeriodeChoices);

    // question 8
    final woningMeterChoices = List<ChoiceDefinition>.empty(growable: true);
    woningMeterChoices.add(ChoiceDefinition(
      choiceNumber: 1,
      choiceText: Translations.textStatic('question08Choice01Text', pageKey),
    ));
    woningMeterChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question08Choice02Text', pageKey)));
    woningMeterChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question08Choice03Text', pageKey)));
    woningMeterChoices.add(ChoiceDefinition(
      choiceNumber: 4,
      choiceText: Translations.textStatic('question08Choice04Text', pageKey),
      resetOtherFields: true,
    ));

    final woningMeterQuestion = MultipleChoiceQuestionDefinition(
        questionNumber: 8,
        questionText: Translations.textStatic('question08Text', pageKey),
        hintText: Translations.textStatic('question08HintText', pageKey),
        choiceList: woningMeterChoices);

    // question 9
    final woningVerwarmingChoices = List<ChoiceDefinition>.empty(growable: true);
    woningVerwarmingChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question09Choice01Text', pageKey)));
    woningVerwarmingChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question09Choice02Text', pageKey)));
    woningVerwarmingChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question09Choice03Text', pageKey)));
    woningVerwarmingChoices.add(ChoiceDefinition(choiceNumber: 4, choiceText: Translations.textStatic('question09Choice04Text', pageKey)));

    final woningVerwarmingQuestion = SingleChoiceQuestionDefinition(
        questionNumber: 9, questionText: Translations.textStatic('question09Text', pageKey), choiceList: woningVerwarmingChoices);

    // question 10
    final woningOnroerendeGoederenChoices = List<ChoiceDefinition>.empty(growable: true);
    woningOnroerendeGoederenChoices.add(ChoiceDefinition(
      choiceNumber: 1,
      choiceText: Translations.textStatic('question10Choice01Text', pageKey),
    ));
    woningOnroerendeGoederenChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question10Choice02Text', pageKey)));
    woningOnroerendeGoederenChoices
        .add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question10Choice03Text', pageKey), resetOtherFields: true));

    final woningOnroerendeGoederenQuestion = MultipleChoiceQuestionDefinition(
        questionNumber: 10,
        questionText: Translations.textStatic('question10Text', pageKey),
        hintText: Translations.textStatic('question10HintText', pageKey),
        choiceList: woningOnroerendeGoederenChoices);

    // question 11
    final autoBeschikkingChoices = List<ChoiceDefinition>.empty(growable: true);
    autoBeschikkingChoices
        .add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question11Choice01Text', pageKey), nextQuestionNumber: 13));
    autoBeschikkingChoices.add(ChoiceDefinition(
        choiceNumber: 2,
        choiceText: Translations.textStatic('question11Choice02Text', pageKey),
        addTextField: true,
        textFieldHint: Translations.textStatic('question11Choice02HintText', pageKey)));

    final autoBeschikkingQuestion = SingleChoiceQuestionDefinition(
        questionNumber: 11, questionText: Translations.textStatic('question11Text', pageKey), choiceList: autoBeschikkingChoices);

    // question 12
    final autoEigendomChoices = List<ChoiceDefinition>.empty(growable: true);
    autoEigendomChoices.add(ChoiceDefinition(
        choiceNumber: 1,
        choiceText: Translations.textStatic('question12Choice01Text', pageKey),
        addTextField: true,
        textFieldHint: Translations.textStatic('question12ChoiceHintText', pageKey)));
    autoEigendomChoices.add(ChoiceDefinition(
        choiceNumber: 2,
        choiceText: Translations.textStatic('question12Choice02Text', pageKey),
        addTextField: true,
        textFieldHint: Translations.textStatic('question12ChoiceHintText', pageKey)));
    autoEigendomChoices.add(ChoiceDefinition(
        choiceNumber: 3,
        choiceText: Translations.textStatic('question12Choice03Text', pageKey),
        addTextField: true,
        textFieldHint: Translations.textStatic('question12ChoiceHintText', pageKey)));
    autoEigendomChoices.add(ChoiceDefinition(
        choiceNumber: 4,
        choiceText: Translations.textStatic('question12Choice04Text', pageKey),
        addTextField: true,
        textFieldHint: Translations.textStatic('question12ChoiceHintText', pageKey)));
    autoEigendomChoices.add(ChoiceDefinition(
        choiceNumber: 5,
        choiceText: Translations.textStatic('question12Choice05Text', pageKey),
        addTextField: true,
        textFieldHint: Translations.textStatic('question12ChoiceHintText', pageKey)));
    autoEigendomChoices.add(ChoiceDefinition(
        choiceNumber: 6,
        choiceText: Translations.textStatic('question12Choice06Text', pageKey),
        addTextField: true,
        textFieldHint: Translations.textStatic('question12ChoiceHintText', pageKey)));

    final autoEigendomQuestion = MultipleChoiceQuestionDefinition(
        questionNumber: 12,
        questionText: Translations.textStatic('question12Text', pageKey),
        hintText: Translations.textStatic('question12HintText', pageKey),
        choiceList: autoEigendomChoices);

    // question 13
    final vervoerChoices = List<ChoiceDefinition>.empty(growable: true);
    vervoerChoices.add(ChoiceDefinition(
      choiceNumber: 1,
      choiceText: Translations.textStatic('question13Choice01Text', pageKey),
    ));
    vervoerChoices.add(ChoiceDefinition(
      choiceNumber: 2,
      choiceText: Translations.textStatic('question13Choice02Text', pageKey),
    ));
    vervoerChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: Translations.textStatic('question13Choice03Text', pageKey)));
    vervoerChoices.add(ChoiceDefinition(
      choiceNumber: 4,
      choiceText: Translations.textStatic('question13Choice04Text', pageKey),
    ));
    vervoerChoices.add(ChoiceDefinition(choiceNumber: 5, choiceText: Translations.textStatic('question13Choice05Text', pageKey), resetOtherFields: true));
    final vervoerQuestion = MultipleChoiceQuestionDefinition(
        questionNumber: 13,
        questionText: Translations.textStatic('question13Text', pageKey),
        hintText: Translations.textStatic('question13HintText', pageKey),
        choiceList: vervoerChoices);

    // question 14
    final hondenChoices = List<ChoiceDefinition>.empty(growable: true);
    hondenChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: '0'));
    hondenChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: '1'));
    hondenChoices.add(ChoiceDefinition(choiceNumber: 3, choiceText: '2'));
    hondenChoices.add(ChoiceDefinition(choiceNumber: 4, choiceText: Translations.textStatic('question14Choice04Text', pageKey)));

    final hondenQuestion =
        SingleChoiceQuestionDefinition(questionNumber: 14, questionText: Translations.textStatic('question14Text', pageKey), choiceList: hondenChoices);

    // question 15
    final onderwijsChoices = List<ChoiceDefinition>.empty(growable: true);
    onderwijsChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question15Choice01Text', pageKey)));
    onderwijsChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question15Choice02Text', pageKey), nextQuestionNumber: 17));

    final onderwijsQuestion = SingleChoiceQuestionDefinition(
        questionNumber: 15,
        questionText: Translations.textStatic('question15Text', pageKey),
        hintText: Translations.textStatic('question15HintText', pageKey),
        choiceList: onderwijsChoices);

    // question 16
    final onderwijsPersonenQuestion = PersonEducationChoiceQuestion(
        questionNumber: 16,
        questionText: Translations.textStatic('question16Text', pageKey),
        hintText: Translations.textStatic('question16HintText', pageKey),
        questionNumberPersons: 3);

    // question 17
    final vakantieChoices = List<ChoiceDefinition>.empty(growable: true);
    vakantieChoices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question17Choice01Text', pageKey)));
    vakantieChoices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question17Choice02Text', pageKey)));

    final vakantieQuestion = SingleChoiceQuestionDefinition(
        questionNumber: 17,
        questionText: _parseTextAndFillStartAndEndDate(Translations.textStatic('question17Text', pageKey), startDate, endDate),
        hintText: Translations.textStatic('question17HintText', pageKey),
        choiceList: vakantieChoices);

    // question 18
    final vakantie2Choices = List<ChoiceDefinition>.empty(growable: true);
    vakantie2Choices.add(ChoiceDefinition(choiceNumber: 1, choiceText: Translations.textStatic('question18Choice01Text', pageKey)));
    vakantie2Choices.add(ChoiceDefinition(choiceNumber: 2, choiceText: Translations.textStatic('question18Choice02Text', pageKey)));

    final vakantie2Question = SingleChoiceQuestionDefinition(
        questionNumber: 18,
        questionText: Translations.textStatic('question18Text', pageKey),
        hintText: Translations.textStatic('question18HintText', pageKey),
        choiceList: vakantie2Choices);

    questionList.add(ageQuestion);
    questionList.add(livingSituationQuestion);
    questionList.add(personInfoQuestion);
    questionList.add(woningQuestion);
    questionList.add(woningSoortQuestion);
    questionList.add(woningGedeelteQuestion);
    questionList.add(woningPeriodeQuestion);
    questionList.add(woningMeterQuestion);
    if (language.toLowerCase() == 'nl') {
      questionList.add(woningVerwarmingQuestion);
    }
    questionList.add(woningOnroerendeGoederenQuestion);
    questionList.add(autoBeschikkingQuestion);
    questionList.add(autoEigendomQuestion);
    questionList.add(vervoerQuestion);
    questionList.add(hondenQuestion);
    questionList.add(onderwijsQuestion);
    questionList.add(onderwijsPersonenQuestion);
    questionList.add(vakantieQuestion);
    questionList.add(vakantie2Question);
  }

  void _createColorList() {
    colorList = <String, Color>{};
    colorList[questionnaireCaptionColorText] = const Color.fromARGB(255, 20, 26, 36);
    colorList[questionnaireButtonColorText] = ColorPallet.lightBlueWithOpacity;
    colorList[questionnairePrimaryTextColorText] = const Color.fromARGB(255, 20, 26, 36);
    colorList[questionnaireBackgroundColorText] = const Color(0xFF00A1CD);
    colorList[questionnaireProgressIndicatorColorText] = ColorPallet.lightGreen;
    colorList[questionnaireSaveButtonColorText] = ColorPallet.lightGreen;
    colorList[questionnaireProgressActiveDotColor] = ColorPallet.lightGreen;
    colorList[questionnaireProgressDotColor] = ColorPallet.lightGray;
  }

  void _createTextList() {
    textList = <String, String>{};
    textList[questionnaireCaptionText] = Translations.textStatic('captionText', pageKey);
    textList[questionnaireQuestionText] = Translations.textStatic('questionText', pageKey);
    textList[questionnaireSaveButtonText] = Translations.textStatic('saveButtonText', pageKey);
    textList[questionnaireNextButtonText] = Translations.textStatic('nextButtonText', pageKey);
    textList[questionnaireConfirmButtonText] = Translations.textStatic('confirmButtonText', pageKey);
    textList[questionnairePersonInfoGenderHintText] = Translations.textStatic('personInfoGenderHintText', pageKey);
    textList[questionnairePersonInfoGenderDialogText] = Translations.textStatic('personInfoGenderDialogText', pageKey);
    textList[questionnairePersonInfoDateOfBirthHintText] = Translations.textStatic('personInfoDateOfBirthHintText', pageKey);
    textList[questionnairePersonInfoYourSelfText] = Translations.textStatic('personInfoYourselfText', pageKey);
    textList[questionnairePersonInfoDateOfBirthText] = Translations.textStatic('personInfoDateOfBirthText', pageKey);
    textList[questionnairePersonInfoRelationHintText] = Translations.textStatic('personInfoRelationHintText', pageKey);
    textList[questionnairePersonInfoRelationText] = Translations.textStatic('personInfoRelationText', pageKey);
    textList[questionnaireLastPageLine1Text] = Translations.textStatic('lastPageLine1Text', pageKey);
    textList[questionnaireLastPageLine2Text] = Translations.textStatic('lastPageLine2Text', pageKey);
    textList[questionnaireLastPageLine3Text] = Translations.textStatic('lastPageLine3Text', pageKey);
    textList[questionnaireAnswersSentText] = Translations.textStatic('lastPageAnswersSentText', pageKey);
    textList[questionnaireCalendarDateInThePastText] = Translations.textStatic('calendarDateInThePastValidationText', pageKey);
  }

  void createQuestionnaire(String language, String startDate, String endDate) {
    (language.toLowerCase() == 'sl') ? _createQuestionListSlovenia(startDate, endDate) : _createQuestionList(language, startDate, endDate);

    _createColorList();
    _createTextList();
  }
}
