import 'package:sqflite/sqflite.dart';

import 'para_data.dart';

class ParaDataTable {
  final Database database;

  const ParaDataTable(this.database);

  static const paraDataTableName = 'paraData';

  Future<void> initialize() async {
    await database.execute('''
          CREATE TABLE $paraDataTableName (
            timestamp INT,
            objectName TEXT,
            action TEXT)
          ''');
  }

  Future<void> insert(ParaData data) async {
    await database.insert(paraDataTableName, data.toJson());
  }

  Future<void> delete(int timestamp) async {
    timestamp = timestamp * 1000; //backend cuts off 3 decimals, due to size contrainst of the date column
    await database.delete(paraDataTableName, where: 'timestamp < ?', whereArgs: [timestamp]);
  }

  Future<List<Map<String, dynamic>>> select() async {
    final _result = await database.query(paraDataTableName);
    return _result;
  }
}
