import 'dart:async';

import 'package:sqflite/sqflite.dart';

import '../../../core/data/database/database_helper.dart';
import '../../../core/data/server/sync_db.dart';

class SearchSuggestions {
  static String tableSearchSuggestionsProducts = 'search_suggestions_products';
  static String tableSearchSuggestionsStores = 'search_suggestions_stores';

  static Future<void> createTables(Database db) async {
    await db.execute('''
          CREATE TABLE $tableSearchSuggestionsProducts (
            product TEXT,
            productCategory TEXT,
            productCode TEXT,
            lastAdded INT,
            count INT)
          ''');

    await db.execute('''
          CREATE TABLE $tableSearchSuggestionsStores (
            storeName TEXT,
            storeType TEXT,
            lastAdded INT,
            count INT)
          ''');
  }

  //Product related code
  static Future<List<Map<String, dynamic>>> findProductCount(String productCode) async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> result = await db.rawQuery('Select count FROM $tableSearchSuggestionsProducts WHERE productCode="$productCode"');
    return result;
  }

  static Future<void> addProduct(String product, String productCategory, String productCode, int newCount) async {
    final db = await DatabaseHelper.instance.database;
    final oldCount = await findProductCount(productCode);
    //Product has not been seen before
    if (oldCount.isEmpty) {
      final data = <String, dynamic>{};
      data['product'] = product;
      data['productCategory'] = productCategory;
      data['productCode'] = productCode;
      data['lastAdded'] = DateTime.now().millisecondsSinceEpoch;
      data['count'] = newCount;
      await db.insert(tableSearchSuggestionsProducts, data);
      // await SyncDatabase.createSync(PRODUCT_TYPE, productCode, CREATED);
      final _syncDatabase = SyncDatabase();
      await _syncDatabase.createSyncSync(dataProductType, productCode, created);
    } else {
      //Product has been seen before
      await db.rawUpdate('UPDATE $tableSearchSuggestionsProducts SET count=${oldCount[0]['count'] + newCount} WHERE productCode="$productCode"');
      await db.rawUpdate('UPDATE $tableSearchSuggestionsProducts SET lastAdded=${DateTime.now().millisecondsSinceEpoch} WHERE productCode="$productCode"');
      // await SyncDatabase.updateSync(PRODUCT_TYPE, productCode, UPDATED);
      final _syncDatabase = SyncDatabase();
      await _syncDatabase.updateSyncSync(dataProductType, productCode, updated);
    }
  }

  static Future<void> removeProduct(String productCode, int newCount) async {
    final db = await DatabaseHelper.instance.database;
    final oldCount = await findProductCount(productCode);
    await db.rawUpdate('UPDATE $tableSearchSuggestionsProducts SET count=${oldCount[0]['count'] - newCount} WHERE productCode="$productCode"');
    // await SyncDatabase.updateSync(PRODUCT_TYPE, productCode, UPDATED);
    final _syncDatabase = SyncDatabase();
    await _syncDatabase.updateSyncSync(dataProductType, productCode, updated);
  }

  static Future<List<Map<String, dynamic>>> getMostRecentProducts() async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> result = await db.rawQuery('Select * FROM $tableSearchSuggestionsProducts ORDER BY lastAdded DESC');
    return result;
  }

  static Future<List<Map<String, dynamic>>> getMostFrequentProducts() async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> result = await db.rawQuery('Select * FROM $tableSearchSuggestionsProducts ORDER BY count DESC');
    return result;
  }

  static Future<List<Map<String, dynamic>>> querySearchProduct(String productCode) async {
    final db = await DatabaseHelper.instance.database;
    return db.query(
      tableSearchSuggestionsProducts,
      where: 'productCode = ?',
      whereArgs: [productCode],
    );
  }

  //Store related code
  static Future<List<Map<String, dynamic>>> findStoreCount(String storeName) async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> result = await db.rawQuery('Select count FROM $tableSearchSuggestionsStores WHERE storeName="$storeName"');
    return result;
  }

  static Future<void> addStore(String storeName, String storeType) async {
    final db = await DatabaseHelper.instance.database;
    final oldCount = await findStoreCount(storeName);
    //Store has not been seen before
    if (oldCount.isEmpty) {
      final data = <String, dynamic>{};
      data['storeName'] = storeName;
      data['storeType'] = storeType;
      data['lastAdded'] = DateTime.now().millisecondsSinceEpoch;
      data['count'] = 1;
      await db.insert(tableSearchSuggestionsStores, data);
      // await SyncDatabase.createSync(STORE_TYPE, storeName, CREATED);
      final _syncDatabase = SyncDatabase();
      await _syncDatabase.createSyncSync(dataStoreType, storeName, created);
    } else {
      //Product has been seen before
      await db.rawUpdate('UPDATE $tableSearchSuggestionsStores SET count=${oldCount[0]['count'] + 1} WHERE storeName="$storeName"');
      await db.rawUpdate('UPDATE $tableSearchSuggestionsStores SET lastAdded=${DateTime.now().millisecondsSinceEpoch} WHERE storeName="$storeName"');
      // await SyncDatabase.updateSync(STORE_TYPE, storeName, UPDATED);
      final _syncDatabase = SyncDatabase();
      await _syncDatabase.updateSyncSync(dataStoreType, storeName, updated);
    }
  }

  static Future<List<Map<String, dynamic>>> getMostRecentStores() async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> result = await db.rawQuery('Select * FROM $tableSearchSuggestionsStores ORDER BY lastAdded DESC');
    return result;
  }

  static Future<List<Map<String, dynamic>>> getMostFrequentStores() async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> result = await db.rawQuery('Select * FROM $tableSearchSuggestionsStores ORDER BY count DESC');
    return result;
  }

  static Future<List<Map<String, dynamic>>> querySearchStore(String storeName) async {
    final db = await DatabaseHelper.instance.database;
    return db.query(
      tableSearchSuggestionsStores,
      where: 'storeName = ?',
      whereArgs: [storeName],
    );
  }
}
