import '../../../core/data/database/tables/product.dart';
import 'util/load_match_list.dart';

class ProductController {
  Future<void> addNewProduct(String productName, String category) async {
    final matchedProduct = await TableProduct().query(productName, category);
    if (matchedProduct.isEmpty) {
      await TableProduct().insert(productName, category);
      await loadProductMatchList();
    }
  }
}
