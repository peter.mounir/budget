extension DiacriticsAwareString on String {
  static const diacritics = 'àáâãäåòóôõöøèéêëðçìíîïùúûüñšÿýž';
  static const nonDiacritics = 'aaaaaaooooooeeeeeciiiiuuuunsyyz';

  String get withoutDiacriticalMarks =>
      this.splitMapJoin('', onNonMatch: (char) => char.isNotEmpty && diacritics.contains(char) ? nonDiacritics[diacritics.indexOf(char)] : char);
}
