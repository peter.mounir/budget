import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class ReceiptListState extends Model {
  static ReceiptListState of(BuildContext context) => ScopedModel.of<ReceiptListState>(context);

  void notify() {
    notifyListeners();
  }
}
