import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../../../features/filter_drawer/state/filter.dart';
import '../../../features/para_data/para_data_scoped_model.dart';
import '../../../features/spotlight_tutorial/controller/receipt_list_tutorial.dart';
import '../controller/util/filter_util.dart';

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton2 = GlobalKey();
GlobalKey keyButton3 = GlobalKey();

class TopBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorPallet.primaryColor,
      height: 50.0 * x,
      child: ScopedModelDescendant<FilterState>(
        builder: (_, __, filterState) {
          if (filterState.isSearchMode) {
            return SearchEnabled();
          }
          return SearchDisabled();
        },
      ),
    );
  }
}

class SearchDisabled extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(width: 22.0 * x),
        Text(
          Translations.textStatic('expensesPage', 'Spending'),
          style: TextStyle(
            color: Colors.white,
            fontSize: 22.0 * f,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(width: 10 * x),
        InkWell(
            key: keyButton1,
            onTap: () {
              ParaDataScopedModel.of(context).onTap('InkWell', 'showTutorialReceiptlist');
              showTutorial(context);
            },
            child: Icon(Icons.info, color: Colors.white, size: 28.0 * x)),
        Expanded(child: Container()),
        InkWell(
          onTap: () {
            FilterState.of(context).enableSearchMode();
          },
          child: Icon(
            Icons.search,
            key: keyButton2,
            size: 37.0 * f,
            color: Colors.white,
          ),
        ),
        SizedBox(width: 6.0 * x),
        ScopedModelDescendant<FilterState>(
          builder: (_, __, filterState) => InkWell(
            key: keyButton3,
            onTap: () {
              ParaDataScopedModel.of(context).onTap('InkWell', 'openFilterDrawer');
              filterState.showDrawer();
            },
            child: FilterUtil().getIcon(filterState),
          ),
        ),
        SizedBox(width: 22.0 * x),
      ],
    );
  }
}

class SearchEnabled extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(width: 12.0 * x),
        SizedBox(
          width: 350.0 * x,
          child: TextField(
            cursorColor: Colors.white,
            onChanged: FilterState.of(context).setSearchInput,
            autofocus: true,
            style: TextStyle(color: Colors.white, fontSize: 20.0 * f),
            decoration: InputDecoration(
              border: InputBorder.none,
              prefixIcon: Icon(
                Icons.search,
                color: ColorPallet.veryLightBlue,
                size: 24 * x,
              ),
              hintText: Translations.textStatic('findTransactions', 'Spending'),
              hintStyle: TextStyle(color: ColorPallet.veryLightBlue, fontSize: 18.0 * f),
            ),
          ),
        ),
        InkWell(
          onTap: FilterState.of(context).disableSearchMode,
          child: Icon(
            Icons.close,
            color: Colors.white,
            size: 33.0 * x,
          ),
        ),
      ],
    );
  }
}
