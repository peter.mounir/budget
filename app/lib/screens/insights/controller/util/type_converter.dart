import 'package:intl/intl.dart';

String doubleToString(double input) {
  if (input == null) {
    return '';
  }
  return input.toString();
}

String dateTimeToString(DateTime input) {
  if (input == null) {
    return '';
  }
  return DateFormat('yyyy-MM-dd').format(input);
}

int dateTimeToInt(DateTime input) {
  if (input == null) {
    return 0;
  }
  return int.parse(DateFormat('yyyyMMdd').format(input));
}
