import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:toast/toast.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/state/translations.dart';
import '../state/receipt_state.dart';

GlobalKey keyButton6 = GlobalKey();

class DiscountInput extends StatefulWidget {
  @override
  _DiscountInputState createState() => _DiscountInputState();
}

class _DiscountInputState extends State<DiscountInput> {
  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    var inputText = Translations.textStatic('discount', 'Manual_Entry');
    final receiptState = ReceiptState.of(context);
    final products = receiptState.receipt.products;
    final hasDiscount = products.discountAmount != 0.0 || products.discountPercentage != 0.0;
    if (hasDiscount) {
      inputText = products.discountAmount != 0.0 ? '-${products.discountAmount.toString()}' : '-${products.discountPercentage.toString()}%';
    }

    return Stack(
      key: keyButton6,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0 * x, vertical: 8.0 * y),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.54,
            decoration: BoxDecoration(border: Border.all(color: ColorPallet.lightGray, width: 1.7 * x), borderRadius: BorderRadius.circular(12)),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10.0 * x, vertical: 10.0 * y),
              child: InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      routeSettings: const RouteSettings(name: 'DiscountDialog'),
                      builder: (BuildContext context) {
                        return DiscountDialog(receiptState);
                      });
                },
                child: Row(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.tags, color: ColorPallet.darkTextColor, size: 18 * f),
                    SizedBox(width: 17.0 * x),
                    SizedBox(
                      width: 160 * x,
                      height: 22 * y,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 160 * x,
                            height: 15 * y,
                            child: AutoSizeText(
                              inputText,
                              style: TextStyle(
                                  color: hasDiscount ? ColorPallet.darkTextColor : ColorPallet.midGray, fontWeight: FontWeight.w500, fontSize: 18 * f),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 48 * x,
          top: 0,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 6.0 * x),
            color: Colors.white,
            child: Text(translations.text('discount'), style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w700, fontSize: 14 * f)),
          ),
        ),
      ],
    );
  }
}

class DiscountDialog extends StatefulWidget {
  final ReceiptState receiptState;

  const DiscountDialog(this.receiptState);

  @override
  __DiscountDialogState createState() => __DiscountDialogState();
}

class __DiscountDialogState extends State<DiscountDialog> {
  double discountPercentage;
  double discountAmount;

  final percentageController = TextEditingController();
  final amountController = TextEditingController();

  bool isValidDouble({@required String value, @required double maxValue}) {
    try {
      final _price = double.parse(value);
      if (_price >= 0 && _price <= maxValue) {
        return true;
      }
    } on Exception {
      print('Input error: $Exception');
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');
    final products = widget.receiptState.receipt.products;

    return SimpleDialog(
      contentPadding: const EdgeInsets.all(0),
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              color: ColorPallet.pink,
              height: 68 * y,
              width: 400 * x,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 20 * y),
                  Padding(
                    padding: EdgeInsets.only(left: 35.0 * x),
                    child: Text(translations.text('enterDiscount'), style: TextStyle(fontSize: 25 * f, fontWeight: FontWeight.w500, color: Colors.white)),
                  ),
                ],
              ),
            ),
            SizedBox(height: 40 * y),
            Row(
              children: <Widget>[
                SizedBox(width: 53 * x),
                Column(
                  children: <Widget>[
                    Text(translations.text('precentage'), style: TextStyle(fontSize: 17 * f, fontWeight: FontWeight.w500, color: ColorPallet.darkTextColor)),
                    SizedBox(height: 10 * y),
                    Row(
                      children: <Widget>[
                        SizedBox(
                            width: 20 * x,
                            height: 45 * y,
                            child: Text('-', style: TextStyle(fontSize: 16 * f, fontWeight: FontWeight.w700, color: ColorPallet.darkTextColor))),
                        SizedBox(
                          width: 40 * x,
                          height: 70 * y,
                          child: TextField(
                            controller: percentageController,
                            onChanged: (newPercentage) {
                              newPercentage = newPercentage.replaceAll(',', '.');
                              if (isValidDouble(value: newPercentage, maxValue: 100)) {
                                discountPercentage = double.parse(newPercentage);
                                discountAmount = null;
                              } else {
                                Toast.show(translations.text('invalidInput'), context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                              }
                            },
                            onTap: () {
                              setState(amountController.clear);
                            },
                            keyboardType: const TextInputType.numberWithOptions(decimal: true),
                            decoration: InputDecoration(hintText: products.discountPercentage.toString()),
                          ),
                        ),
                        SizedBox(
                            width: 10 * x,
                            height: 45 * y,
                            child: Text('%', style: TextStyle(fontSize: 16 * f, fontWeight: FontWeight.w700, color: ColorPallet.darkTextColor))),
                      ],
                    ),
                  ],
                ),
                SizedBox(width: 25 * x),
                Text(translations.text('or'), style: TextStyle(fontSize: 17 * f, fontWeight: FontWeight.w500, color: ColorPallet.darkTextColor)),
                SizedBox(width: 35 * x),
                Column(
                  children: <Widget>[
                    Text(translations.text('total2'), style: TextStyle(fontSize: 17 * f, fontWeight: FontWeight.w500, color: ColorPallet.darkTextColor)),
                    SizedBox(height: 10 * y),
                    Row(
                      children: <Widget>[
                        SizedBox(
                            width: 20 * x,
                            height: 45 * y,
                            child: Text('-', style: TextStyle(fontSize: 16 * f, fontWeight: FontWeight.w700, color: ColorPallet.darkTextColor))),
                        SizedBox(
                          width: 40 * x,
                          height: 70 * y,
                          child: TextField(
                            controller: amountController,
                            onChanged: (newAmount) {
                              newAmount = newAmount.replaceAll(',', '.');
                              if (isValidDouble(value: newAmount, maxValue: 100000000)) {
                                discountAmount = double.parse(newAmount);
                                discountPercentage = null;
                              } else {
                                if (newAmount != '0' && newAmount != '0.' && newAmount != '0,' && newAmount != ',' && newAmount != '.') {
                                  Toast.show(translations.text('invalidInput'), context, duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
                                }
                              }
                            },
                            onTap: () {
                              setState(percentageController.clear);
                            },
                            keyboardType: const TextInputType.numberWithOptions(decimal: true),
                            decoration: InputDecoration(hintText: products.discountAmount.toString()),
                          ),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
            SizedBox(height: 30 * y),
            SizedBox(
              width: 400 * x,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      translations.text('cancel'),
                      style: TextStyle(fontSize: 16 * f, color: ColorPallet.pink),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      if (discountPercentage != null) {
                        products.discountPercentage = discountPercentage;
                        products.discountAmount = 0.0;
                        widget.receiptState.notify();
                      } else if (discountAmount != null) {
                        products.discountPercentage = 0.0;
                        products.discountAmount = discountAmount;
                        widget.receiptState.notify();
                      }
                      Navigator.pop(context);
                    },
                    child: Text(
                      translations.text('add'),
                      style: TextStyle(fontSize: 16 * f, color: ColorPallet.pink),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 10 * y)
          ],
        )
      ],
    );
  }
}
