import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/controller/util/responsive_ui.dart';
import '../../../core/data/database/tables/receipt_product.dart';
import '../../../core/model/color_pallet.dart';
import '../../../core/model/receipt_product.dart';
import '../../../core/state/translations.dart';
import '../../insights/controller/util/type_converter.dart';
import '../../search/controller/add_search_suggestion.dart';
import '../../search/search.dart';
import '../state/receipt_state.dart';
import 'add_product_dialog.dart';

GlobalKey keyButton5 = GlobalKey();

class AddProductButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final translations = Translations(context, 'Manual_Entry');

    return ScopedModelDescendant<ReceiptState>(builder: (context, _, receiptState) {
      return InkWell(
        key: keyButton5,
        onTap: () async {
          final result = await Navigator.push(
            context,
            MaterialPageRoute(
              settings: const RouteSettings(name: 'SearchWidget'),
              builder: (context) => const SearchWidget(true),
            ),
          );

          if (result != null) {
            final date = receiptState.receipt.date ?? DateTime.now();
            final productDate = dateTimeToString(date);
            final coicop = await ReceiptProductTable().getCoicop(result[1]);
            final product = ReceiptProduct(name: result[0], category: result[1], coicop: coicop, productDate: productDate);
            await SearchSuggestions.addProduct(product.name, product.category, coicop, product.count);

            await showDialog(
              context: context,
              routeSettings: const RouteSettings(name: 'ProductDialog'),
              builder: (BuildContext context) {
                return ProductDialog(product, receiptState);
              },
            );
          }
        },
        child: DottedBorder(
          color: ColorPallet.lightGray,
          strokeWidth: 1.8,
          dashPattern: const [6, 6],
          radius: const Radius.circular(10),
          strokeCap: StrokeCap.round,
          borderType: BorderType.RRect,
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            child: SizedBox(
              height: 50 * y,
              width: 365 * x,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(width: 21 * x),
                  Text(
                    translations.text('addNewServiceProductOrDiscount'),
                    style: TextStyle(color: ColorPallet.darkTextColor, fontWeight: FontWeight.w600, fontSize: 15 * f),
                  ),
                  SizedBox(width: 31 * x),
                  const Icon(
                    Icons.add,
                    color: ColorPallet.darkTextColor,
                  )
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
