import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../core/controller/util/responsive_ui.dart';
import '../../core/model/color_pallet.dart';
import '../../core/model/receipt.dart';
import '../../core/widget/close_page_warning_dialog.dart';
import '../../features/para_data/para_data_name.dart';
import '../../features/para_data/para_data_scoped_model.dart';
import '../../features/spotlight_tutorial/controller/manual_entry_tutorial.dart';
import 'state/receipt_state.dart';
import 'widget/add_product_button.dart';
import 'widget/bottom_bar_widget.dart';
import 'widget/date_input.dart';
import 'widget/discount_input.dart';
import 'widget/receipt_products_list.dart';
import 'widget/shop_attributes_input.dart';
import 'widget/shop_input.dart';
import 'widget/top_bar_widget.dart';

class ManualEntryScreen extends StatefulWidget with ParaDataName {
  final Receipt receiptModel;
  final bool isUpdate;

  @override
  String get name => 'ManualEntryScreen';

  const ManualEntryScreen(this.receiptModel, this.isUpdate);

  @override
  _ManualEntryScreenState createState() => _ManualEntryScreenState();
}

class _ManualEntryScreenState extends State<ManualEntryScreen> {
  @override
  void initState() {
    super.initState();
    initTargets(keyButton1, keyButton2, keyButton3, keyButton4, keyButton5, keyButton6, keyButton7, keyButton8);
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: ColorPallet.pink,
    ));
  }

  @override
  Widget build(BuildContext context) {
    final scrollController = ScrollController();
    return ScopedModel<ReceiptState>(
      model: ReceiptState(widget.receiptModel),
      child: Scaffold(
        appBar: AppBar(
          leading: InkWell(
            onTap: () async {
              await ParaDataScopedModel.of(context).onTap('InkWell', 'closingScreenWarning');
              await showDialog(
                routeSettings: const RouteSettings(name: 'ClosingScreenWarning'),
                context: context,
                builder: (BuildContext context) {
                  return ClosingScreenWarning();
                },
              );
            },
            child: Icon(Icons.close, color: Colors.white, size: 28 * x),
          ),
          titleSpacing: 0,
          title: TopBarWidget(scrollController, widget.isUpdate),
          backgroundColor: ColorPallet.pink,
          systemOverlayStyle: SystemUiOverlayStyle.light,
        ),
        body: SafeArea(
          child: Container(
            color: Colors.white,
            child: WillPopScope(
              onWillPop: () async {
                await showDialog(
                  context: context,
                  routeSettings: const RouteSettings(name: 'ClosingScreenWarning'),
                  builder: (BuildContext context) {
                    return ClosingScreenWarning();
                  },
                );
                return false;
              },
              child: ScopedModelDescendant<ReceiptState>(
                builder: (context, child, receiptState) {
                  return Column(
                    children: [
                      MiddleSection(scrollController, receiptState),
                      BottomBarWidget(),
                    ],
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class MiddleSection extends StatelessWidget {
  final ScrollController scollController;
  final ReceiptState receiptState;

  const MiddleSection(this.scollController, this.receiptState);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        controller: scollController,
        child: Column(
          children: [
            SizedBox(height: 10 * y),
            ShopInput(),
            SizedBox(height: 10 * y),
            DateInput(),
            SizedBox(height: 10 * y),
            ShopAttributesInput(),
            SizedBox(height: 10 * y),
            SizedBox(
              height: 275 * y,
              child: ListView(
                children: <Widget>[
                  ReceiptProductsList(scollController),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 22.0 * x),
                    child: AddProductButton(),
                  ),
                  SizedBox(height: 7 * y),
                ],
              ),
            ),
            SizedBox(height: 20 * y),
          ],
        ),
      ),
    );
  }
}
