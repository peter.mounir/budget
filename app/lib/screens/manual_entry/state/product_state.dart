import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../core/model/receipt_product.dart';

class ProductState extends Model {
  ReceiptProduct product;

  ProductState({@required this.product});

  static ProductState of(BuildContext context) => ScopedModel.of<ProductState>(context);

  void notify() {
    notifyListeners();
  }
}
