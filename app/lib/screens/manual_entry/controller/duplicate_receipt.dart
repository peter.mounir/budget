import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

import '../../../core/data/database/tables/receipt.dart';
import '../../../core/data/server/sync.dart';
import '../../../core/data/server/sync_db.dart';
import '../../../core/model/receipt.dart';
import '../../receipt_list/state/receipt_list_state.dart';

Future<void> duplicateReceipt(BuildContext context, Receipt receipt) async {
  receipt.id = const Uuid().v1();
  receipt.date = DateTime.now();
  for (final product in receipt.products.content) {
    product.id = const Uuid().v1();
  }
  await ReceiptTable().insert(receipt);
  ReceiptListState.of(context).notify();
  final _syncDatabase = SyncDatabase();
  await _syncDatabase.createSyncSync(dataReceiptType, receipt.id, created);
  await Synchronise.synchronise();
}
