package nl.cbs.householdbudgetsurvey.receipt_scanner;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;

import nl.cbs.householdbudgetsurvey.receipt_scanner.model.Quad;
import nl.cbs.householdbudgetsurvey.receipt_scanner.view.PaperRectangle;
import nl.cbs.householdbudgetsurvey.receipt_scanner.view.PaperRectangleCallback;

import org.javatuples.Octet;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import nl.cbs.householdbudgetsurvey.receipt_scanner.model.Quad;

public class CropActivity extends Activity implements PaperRectangleCallback {
    private static final String TAG = "DocScanner::CropAct";

    public static Quad contour;
    public static Mat image;
    public static int openCVCamWidth;
    public static int openCVCamHeight;

    private Bitmap bm;
    private Quad sharedQuad;

    private Mat croppedGrayImage;
    private Mat croppedThresholdImage;

    ImageView scanPreviewImageView;

    private ImageButton nextButton;
    private ImageButton prevButton;

    private ConstraintLayout selectOnlyProductsPopup;

    private double grayMeanThreshold = 170;

    private int leadingPadding;

    private int topPadding;

    private double imageScaleFactor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop);

        scanPreviewImageView = findViewById(R.id.preview_image_view);
        final PaperRectangle paperRectangle = findViewById(R.id.quad_overlay);
        paperRectangle.registerPaperRectangleCallback(this);
        nextButton = findViewById(R.id.done_crop_button);
        prevButton = findViewById(R.id.back_to_scan_button);

        scanPreviewImageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //Remove it here unless you want to get this callback for EVERY
                //layout pass, which can get you into infinite loops if you ever
                //modify the layout from within this method.
                scanPreviewImageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                // Rotate image and convert to bitmap:
                Mat img = image;

                Core.flip(img.t(), img, 1);
                bm = Bitmap.createBitmap(img.cols(), img.rows(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(img, bm);

                // Scale, Rotate and Transform the contour to fit the screen/the receipt
                Quad drawableContour = prepareContour(contour, scanPreviewImageView, bm.getWidth(), bm.getHeight());

                // Show the receipt image
                scanPreviewImageView.setImageBitmap(bm);

                // Present the quad contour on the image
                drawableContour.resetCorrectPointPerCorner();
                sharedQuad = drawableContour;

                paperRectangle.previewCorners(sharedQuad);
            }
        });

        nextButton.setOnClickListener((View view) ->  {
            processImagesForPreview(image, sharedQuad);
            if (croppedGrayImage != null && croppedThresholdImage != null) {
                ReceiptPreviewActivity.croppedGrayImage = croppedGrayImage;
                ReceiptPreviewActivity.croppedThresholdedImage = croppedThresholdImage;
                ReceiptPreviewActivity.shouldShowThresholded = shouldShowThresholded(croppedGrayImage);
                ReceiptPreviewActivity.originalImage = image;
                Intent intent = new Intent(getBaseContext(), ReceiptPreviewActivity.class);
                startActivity(intent);
            }
        });

        prevButton.setOnClickListener((View view) ->  {
            finish();
        });

        selectOnlyProductsPopup = findViewById(R.id.selectOnlyProductsPopup);
        Button closePopupButton = findViewById(R.id.closePopupButton);
        closePopupButton.setOnClickListener((View view) ->  {
            selectOnlyProductsPopup.animate().alpha(0);
        });
    }

    private Quad prepareContour(Quad contour, ImageView imageView, int ogImageWidth, int ogImageHeight) {
        Quad drawableContour;
        int imageViewHeight = imageView.getHeight();
        int imageViewWidth = imageView.getWidth();

        int widthDiff = ogImageWidth - imageViewWidth;
        int heightDiff = ogImageHeight - imageViewHeight;

        if (widthDiff > heightDiff) {
            // Image is scaled on width
            this.imageScaleFactor = (double) imageViewWidth / (double) ogImageWidth;
        } else {
            // Image is scaled on height
            this.imageScaleFactor = (double) imageViewHeight / (double) ogImageHeight;
        }

        int newImageWidth = (int) ((double) ogImageWidth * this.imageScaleFactor);
        int newImageHeight = (int) ((double) ogImageHeight * this.imageScaleFactor);

        this.leadingPadding = (imageViewWidth - newImageWidth) / 2;
        this.topPadding = (imageViewHeight - newImageHeight) / 2;

        if (contour != null && contour.contourList.size() > 0) {
            double widthScaling = (double) newImageWidth / (double) openCVCamWidth;
            double heightScaling = (double) newImageHeight / (double) openCVCamHeight;

            // Rotate the points by assigning them to the correct new corners (rotate 90 deg clockwise)
            // Scale with the difference between camera frame and image size
            Point tempLt = new Point(((openCVCamWidth - contour.lb.y) * widthScaling) + leadingPadding, (contour.lb.x * heightScaling) + topPadding);
            Point tempRt = new Point(((openCVCamWidth - contour.lt.y) * widthScaling) + leadingPadding, (contour.lt.x * heightScaling) + topPadding);
            Point tempLb = new Point(((openCVCamWidth - contour.rb.y) * widthScaling) + leadingPadding, (contour.rb.x * heightScaling) + topPadding);
            Point tempRb = new Point(((openCVCamWidth - contour.rt.y) * widthScaling) + leadingPadding, (contour.rt.x * heightScaling) + topPadding);

            contour.lt = tempLt;
            contour.rt = tempRt;
            contour.lb = tempLb;
            contour.rb = tempRb;

            drawableContour = contour;
        } else {
            int padding = 200;
            Point[] points = {
                    new Point(this.leadingPadding + padding, this.topPadding + padding),
                    new Point(this.leadingPadding + newImageWidth - padding, this.topPadding + padding),
                    new Point(this.leadingPadding + newImageWidth - padding, this.topPadding + newImageHeight - padding),
                    new Point(this.leadingPadding + padding, this.topPadding + newImageHeight - padding),
            };

            MatOfPoint simplePoints = new MatOfPoint();
            simplePoints.fromArray(points);
            List<MatOfPoint> contourTemp = new ArrayList<>();
            contourTemp.add(simplePoints);
            drawableContour = new Quad(contourTemp);
        }

        return drawableContour;
    }

    private void processImagesForPreview(Mat image, Quad cornerQuad) {
        // Crop the image
        Mat cropped = cropPicture(image, cornerQuad);
        if (cropped == null) return;

        // Grayscale the image
        Mat mGray = new Mat(cropped.size(), CvType.CV_8UC4);
        Imgproc.cvtColor(cropped, mGray, Imgproc.COLOR_RGBA2GRAY);

        Mat mThreshold = new Mat(mGray.size(), CvType.CV_8UC4);
        Imgproc.threshold(mGray, mThreshold, 150, 255, Imgproc.THRESH_BINARY +  Imgproc.THRESH_OTSU);

        this.croppedGrayImage = mGray;
        this.croppedThresholdImage = mThreshold;
    }

    private boolean shouldShowThresholded(Mat grayImage) {
        Scalar mean = Core.mean(grayImage);
        double meanValue = mean.val[0];
        return meanValue < grayMeanThreshold;
    }

    private Mat cropPicture(Mat sourceImage, Quad cornerQuad) {
        if (cornerQuad == null) { return null; }

        // First, we scale back the corner quad to the original image size (local copy)
        Quad newQuad = prepareContourForCrop(cornerQuad);

        // Calculate the top width using the euclidean distance
        double widthBottom = Math.sqrt(Math.pow(newQuad.rb.x - newQuad.lb.x, 2) + Math.pow(newQuad.rb.y - newQuad.lb.y, 2));
        double widthTop = Math.sqrt(Math.pow(newQuad.rt.x - newQuad.lt.x, 2) + Math.pow(newQuad.rt.y - newQuad.lt.y, 2));
        int maxWidth = (int) Math.max(widthBottom, widthTop);

        // Calculate the bottom width using the euclidean distance
        double heightLeft = Math.sqrt(Math.pow(newQuad.lb.x - newQuad.lt.x, 2) + Math.pow(newQuad.lb.y - newQuad.lt.y, 2));
        double heightRight = Math.sqrt(Math.pow(newQuad.rb.x - newQuad.rt.x, 2) + Math.pow(newQuad.rb.y - newQuad.rt.y, 2));
        int maxHeight = (int) Math.max(heightLeft, heightRight);

        Mat croppedPicture = new Mat(maxHeight, maxWidth, CvType.CV_8UC4);
        Mat srcMat = new Mat(4, 1, CvType.CV_32FC2);
        Mat dstMat = new Mat(4, 1, CvType.CV_32FC2);

        double scaledTopPadding = this.topPadding / this.imageScaleFactor;
        double scaledLeadingPadding = this.leadingPadding / this.imageScaleFactor;

        srcMat.put(0, 0,
            (newQuad.lt.x - scaledLeadingPadding), (newQuad.lt.y - scaledTopPadding),
            (newQuad.rt.x - scaledLeadingPadding), (newQuad.rt.y - scaledTopPadding),
            (newQuad.lb.x - scaledLeadingPadding), (newQuad.lb.y - scaledTopPadding),
            (newQuad.rb.x - scaledLeadingPadding), (newQuad.rb.y - scaledTopPadding));

        dstMat.put(0, 0, 0.0              , 0.0,
                (double) maxWidth, 0.0,
                0.0              , (double) maxHeight,
                (double) maxWidth, (double) maxHeight);

        Mat pt = Imgproc.getPerspectiveTransform(srcMat, dstMat);
        Imgproc.warpPerspective(sourceImage, croppedPicture, pt, croppedPicture.size());
        pt.release();
        srcMat.release();

        dstMat.release();

        return croppedPicture;
    }

    private Quad prepareContourForCrop(Quad cornerQuad) {
        Quad updatedQuad = new Quad();
        updatedQuad.rt.x = cornerQuad.rt.x / this.imageScaleFactor;
        updatedQuad.rt.y = cornerQuad.rt.y / this.imageScaleFactor;
        updatedQuad.rb.x = cornerQuad.rb.x / this.imageScaleFactor;
        updatedQuad.rb.y = cornerQuad.rb.y / this.imageScaleFactor;
        updatedQuad.lt.x = cornerQuad.lt.x / this.imageScaleFactor;
        updatedQuad.lt.y = cornerQuad.lt.y / this.imageScaleFactor;
        updatedQuad.lb.x = cornerQuad.lb.x / this.imageScaleFactor;
        updatedQuad.lb.y = cornerQuad.lb.y / this.imageScaleFactor;
        return updatedQuad;
    }

    @Override
    public void paperRectangleIsAllowed(boolean allowed) {
        if (!allowed) Log.i(TAG, "No");
        nextButton.setEnabled(allowed);

        if (allowed) nextButton.setAlpha(1f);
        else nextButton.setAlpha(.5f);
    }
}
