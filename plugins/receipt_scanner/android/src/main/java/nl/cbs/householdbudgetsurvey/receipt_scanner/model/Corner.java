package nl.cbs.householdbudgetsurvey.receipt_scanner.model;

public enum Corner {
    LEFT_TOP,
    RIGHT_TOP,
    LEFT_BOTTOM,
    RIGHT_BOTTOM,
}