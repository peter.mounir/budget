import 'package:receipt_scanner/models/text_observation.dart';

// Represent a complete scan result returned from iOS/Android
class ScanResult {
  final VisionResult visionResult;

  final VisionSize croppedImageSize;
  final String croppedImageBase64;

  final VisionSize originalImageSize;
  final String originalImageBase64;

  ScanResult(this.visionResult, this.croppedImageSize, this.croppedImageBase64, this.originalImageSize, this.originalImageBase64);

factory ScanResult.fromJson(Map<String, dynamic> json) {
    return ScanResult(
      VisionResult.fromJson(json['visionResult']),
      VisionSize.fromJson(json['croppedImageSize']),
      json['croppedImageBase64'],
      VisionSize.fromJson(json['originalImageSize']),
      json['originalImageBase64'],
    );
  }
}