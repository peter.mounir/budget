import Flutter
import UIKit

public class SwiftReceiptScannerPlugin: NSObject, FlutterPlugin {
    private let RECEIPT_SCANNER_METHOD = "openReceiptScanner"

    private lazy var receiptScannerViewController: ReceiptScannerViewController = {
        let receiptScannerView = ReceiptScannerViewController()
        receiptScannerView.modalPresentationStyle = .overCurrentContext
        return receiptScannerView
    }()

    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "receipt_scanner", binaryMessenger: registrar.messenger())
        let instance = SwiftReceiptScannerPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        guard call.method == self.RECEIPT_SCANNER_METHOD else {
            result(FlutterMethodNotImplemented)
            return
        }

        if let rootViewController = UIApplication.shared.keyWindow?.rootViewController,
           let args = call.arguments as? Dictionary<String, Any>,
           let tips = args["tips"] as? String,
           let tipsData = tips.data(using: .utf8),
           let tipsArray = try? JSONDecoder().decode([String].self, from: tipsData) {

            rootViewController.present(self.receiptScannerViewController, animated: true) { [weak self] in
                guard let strongSelf = self else {
                    result("Unable to start the scanner")
                    return
                }
                
                strongSelf.receiptScannerViewController.startDocumentScanner(withTips: tipsArray) { (results) in
                    DispatchQueue.main.async {
                        rootViewController.dismiss(animated: true)
                    }
                    result(results)
                }
            }
        } else {
            result("Did not have a window")
        }
    }
}
