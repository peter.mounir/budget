//
//  UIImage+base64Encoded.swift
//  Runner
//
//  Created by Marc Wiggerman on 12/05/2020.
//

import Foundation

extension UIImage {
    /// The UIImage encoded as a Base64 encoded string
    var base64Encoded: String {
        get {
            guard let imageData = self.pngData() else { return "" }
            return imageData.base64EncodedString()
        }
    }
}
