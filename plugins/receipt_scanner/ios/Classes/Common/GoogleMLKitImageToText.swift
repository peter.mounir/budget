//
//  GoogleMLKitImageToText.swift
//
//  Created by Marc Wiggerman on 15/06/2020.
//

import Foundation

import MLKitTextRecognition
import MLKitVision

public class GoogleMLKitImageToText: ImageToTextProtocol {
    internal var currentImageBeingAnalysedCropped: UIImage?
    internal var currentImageBeingAnalysedOriginal: UIImage?
    internal var completionHandler: ((String) -> Void)

    required init(onComplete completionHandler: @escaping ((String) -> Void)) {
        self.completionHandler = completionHandler
    }

    /// Starts the text extraction process
    /// - Parameter image: The image which will be used to extract data
    /// Note: the cropped image is used for OCR
    func processImage(croppedImage: UIImage, originalImage: UIImage) {
        let visionImage = VisionImage(image: croppedImage)
        visionImage.orientation = .up
        let textRecognizer = TextRecognizer.textRecognizer()

        textRecognizer.process(visionImage) { result, error in
            guard error == nil, let result = result else {
                self.completionHandler(ScannerError.scanFailed.rawValue)
                return
            }

            if let resultString = self.getJsonStringResults(result, croppedImage: croppedImage, originalImage: originalImage) {
                self.completionHandler(resultString)
            }
        }
    }

    /// Creates a JSON string from VNRecognizedTextObservation
    /// - Parameters:
    ///   - results: Vision results
    ///   - sourceImage: The image in which the vision observations are made
    fileprivate func getJsonStringResults(_ results: Text, croppedImage: UIImage, originalImage: UIImage) -> String? {
        let visionResults = self.preprocessResults(results, sourceImage: croppedImage)
        let scanResult = ScanResult(visionResult: visionResults, croppedImage: croppedImage, originalImage: originalImage)

        do {
            let jsonData = try JSONEncoder().encode(scanResult)
            let jsonString = String(data: jsonData, encoding: .utf8)
            return jsonString
        } catch {
            NSLog("The results could not be converted to JSON")
            return nil
        }
    }

    /// Preprocesses the results by getting the top candidate (with the most confidence)
    /// after which they are turned into TextObservations
    /// - Parameters:
    ///   - results: Vision results
    ///   - sourceImage: The image in which the vision observations are made
    fileprivate func preprocessResults(_ results: Text, sourceImage: UIImage) -> VisionResult {
        var textObservations = [VisionResult.TextObservation]()

        let imageWidth = sourceImage.size.width * sourceImage.scale
        let imageHeight = sourceImage.size.height * sourceImage.scale

        for block in results.blocks {
            for line in block.lines {
                textObservations.append(
                    VisionResult.TextObservation(
                        text: line.text,
                        confidence: -1.0,
                        normalizedRect: self.normalizeRect(
                            sourceRect: line.frame,
                            imageWidth: imageWidth,
                            imageHeight: imageHeight
                        )
                    )
                )
            }
        }

        return VisionResult(textObservations: textObservations)
    }

    private func normalizeRect(sourceRect: CGRect, imageWidth: CGFloat, imageHeight: CGFloat) -> VisionResult.VisionRect {
        let newX = sourceRect.minX / imageWidth;
        let newY = sourceRect.minY / imageHeight;
        let newWidth = sourceRect.width / imageWidth;
        let newHeight = sourceRect.height / imageHeight;
        return VisionResult.VisionRect(rect: CGRect(x: newX, y: newY, width: newWidth, height: newHeight), visionMethod: .googleMLKitVision)
    }
}
